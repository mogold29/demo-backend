import { JobPriority } from "../models/jobPriority";
import { JobPriorityQuery } from "../queries/jobPriority";
import { Response } from "../models/response";
import { ExpressRequest } from "../models/express";
import requestCheck from "../middleware/requestCheck";
import strings from "../strings";
import { uuid } from "uuidv4";

const jobPriorityQuery = new JobPriorityQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc JobPriorityClass manages job priorities
 */
export class JobPriorityClass {

    /**
     * @public
     * @async
     * @summary Create a new priority
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.priorityName - the name of the priority
     * @param { string } req.body.priorityColor - a color hex value
     * @param { boolean } req.body.isAlerting - should a job alert users when this priority is used
     * @returns { Response } strings.priorityCreatedSuccessfully
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["priorityName", "priorityColor", "isAlerting"]);
                const existingPriorities = await jobPriorityQuery.get();
                const isExisting = existingPriorities.filter((priority: JobPriority) => priority.priorityName === req.body.priorityName);
                if(isExisting.length > 0) {
                    resolve({status: "error", message: strings.priorityAlreadyExists});
                }
                const priority: JobPriority = {
                    priorityId: uuid(),
                    priorityName: req.body.priorityName,
                    priorityColor: req.body.priorityColor || "#FFFFFF",
                    isAlerting: req.body.isAlerting || false,
                    priorityOrder: req.body.priorityOrder || 5
                }
                req.data = priority;
                const createQuery = await jobPriorityQuery.create(req);
                resolve({status: "success", message: strings.priorityCreatedSuccessfully});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @param
     * @async
     * @summary Update a priority
     * @param { ExpressRequest } req - the default request obejct
     * @param { string } req.body.priorityId - the priortityId to update
     * @returns { Response } strings.priorityUpdatedSuccess
     */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["priorityId"]);
                req.data = {priorityId: req.body.priorityId}
                const existing = await jobPriorityQuery.getSingle(req);
                let updated: any = {};
                
                for(let[key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        updated[key] = req.body[key]
                    }
                }
                
                if(Object.keys(updated).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }

                req.data = {
                    update: updated,
                    priorityId: req.body.priorityId
                }

                const update = await jobPriorityQuery.update(req);
                resolve({status: "success", message: strings.priorityUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get all job priorities
     * @returns { Response } an array of priorities in the res.data, can be zero length
     */
    public async get() {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const priorities = await jobPriorityQuery.get();
                resolve({status: "success", data: {priorities}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default JobPriorityClass;