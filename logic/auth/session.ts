import { ExpressRequest } from "../../models/express";
import requestCheck from "../../middleware/requestCheck";
import mysql from "../../middleware/dateFormat";
import { uuid } from "uuidv4";
import { SessionQuery } from "../../queries/auth/session";
import { Session } from "../../models/session";
import { Response } from "../../models/response";
import strings from "../../strings";

const sessionQuery = new SessionQuery();

export class SessionClass {

    /**
    * @public
    * @async
    * @summary Create a new session
    * @param req
    * @return a session token
    */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const session: Session = {
                    userId: req.authenticate.userId,
                    sessionToken: uuid(),
                    sessionExpiry: new Date(),
                    sessionDateCreated: new Date(),
                    sessionDeviceId: req.authenticate.deviceId,
                    sessionIp: req.header("x-forwarded-for") || req.connection.remoteAddress,
                    ownerId: req.authenticate.userId
                }
                req.data = session;
                await sessionQuery.create(req);
                resolve({status: "success", data: {token: session.sessionToken}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary destroy a session
    * @param { ExpressRequest } req - default request object
    * @return strings.sessionEndSuccess
    */
    public async destroy(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await sessionQuery.end(req);
                resolve({status: "success", message: strings.sessionEndSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Check if a session is valid
    * @param { ExpressRequest } req - The default request object
    * @param { string } req.body.
    * @return if successfull a session token otherwise an error message
    */
    public async check(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                resolve({status: "success", data: req.authenticate.sessionToken});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}