import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import { env } from "../env/env";
import { Address as IdealPostcodesAddres, AddressSuggestion as IdealPostcodesAutocomplete } from "@ideal-postcodes/api-typings/index";
import { Client } from "@ideal-postcodes/core-node";
import { Address } from "../models/address";
import { AddressQuery } from "../queries/address";
const addressQuery = new AddressQuery();
const idealPostcodesClient = new Client({api_key: env.idealPostcodesApiKey});

/**
 * @class
 * @classdesc Class for managing interactions with ideal-postcodes api
 * @author Mo Gold
 */
export class IdealPostcodeClass {

    /**
    * @public
    * @async
    * @summary Search for addresses matching a search parameter
    * @param { ExpressRequest } req - the default request object
    * @param { Object } req.body - the default req.body
    * @param { string } req.body.term - the string to search for
    * @return { Response } an array of type IdealPostcodeSearch in the data object
    */
    public async search(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["term"]);
                if (req.body.term.length >= 3) {
                    const get = await idealPostcodesClient.autocomplete.list({
                        query: {
                            api_key: env.idealPostcodesApiKey,
                            licensee: env.idealPostcodeClientToken,
                            query: req.body.term
                        }
                    });
                    resolve({status: "success", data: get.body.result});
                }
                resolve({status: "success", message: `${strings.addressSearchShortString}`});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Get a single address of a udprn
    * @param { ExpressRequest} req - the default request object
    * @param { Object } req.body - the default req.body
    * @param { number } req.body.udprn - the udprn to search for
    * @return { Response } an array with a single address
    */
    public async getAddressByUdprn(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["udprn"]);
                const address: IdealPostcodesAddres = await idealPostcodesClient.lookupUdprn({
                    udprn: req.body.udprn,
                    api_key: env.idealPostcodesApiKey,
                    licensee: env.idealPostcodeClientToken
                });

                const addressFromDb = await this.addAddressToDb(req, address);

                resolve({status: "success", data: addressFromDb});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @private
    * @async
    * @summary Check if address is already in db, if not create a new address
    * @param { ExpressRequest } req - the default request object
    * @param { IdealPostcodeAddress } address - the address to check and or create
    * @return { Address } a single address from the database
    */
    private async addAddressToDb(req: ExpressRequest, address: IdealPostcodesAddres) {
        return new Promise<Address>(async (resolve, reject) => {
        	try {

                req.data = {udprn: address.udprn}

                const isAddressInDb = await addressQuery.getSingleByUdprn(req);

                if (isAddressInDb.length > 0) {
                    resolve(isAddressInDb[0]);
                } 

                const newAddress: Address = {
                    addressLineOne: address.line_1,
                    addressLineTwo: address.line_2,
                    addressLineThree: address.line_3,
                    addressPostcode: address.postcode,
                    addressCity: address.post_town,
                    addressBorugh: address.district,
                    latitude: address.latitude,
                    longitude: address.longitude,
                    addressId: uuid(),
                    ownerId: req.authenticate.userId,
                    udprn: address.udprn
                }

                req.data = newAddress;

                await addressQuery.create(req);
                
                req.data = {udprn: newAddress.udprn}
                const addressFromDb = await addressQuery.getSingleByUdprn(req);
                resolve(addressFromDb[0]);
        		
        	} catch(error) {
        		reject(error);
        	};
        });
    }

}

export default IdealPostcodeClass;