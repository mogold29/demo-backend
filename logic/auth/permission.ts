import { ExpressRequest } from "../../models/express";
import { Response } from "../../models/response";
import { uuid } from "uuidv4";
import strings from "../../strings";
import requestCheck from "../../middleware/requestCheck";
import { Permission } from "../../models/auth/permission";
import { PermissionQuery } from "../../queries/auth/permission";

const permissionQuery = new PermissionQuery();

export class PermissionClass {

    /**
    * @public
    * @async
    * @summary Create a new permission
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.roleId - the roleId to which the permission belongs
    * @param { string } req.body.action - the action the permission is controlling such as create, get
    * @param { string } req.body.attribute - the attribute the permission is prtecting such as customer, job
    * @param { boolean } req.body.allow - should the permission allow this action,
    * @param { boolean } req.body.allowIfManager - should the permission allow if the user is the manager of the record owner
    * @param { boolean } req.body.allowIfOwner - should the permission allow if the user is the reord owner
    * @param { string } req.body.requireId - does the permission required a record Id ro check if the user can do the action
    * @return { Response } strings.permissionCreatedSuccess
    */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["roleId", "action", "attribute", "allow", "allowIfManager", "allowIfOwner", "requireId"]);
                const permission: Permission = {
                    permissionId: uuid(),
                    requireId: req.body.requireId || false,
                    roleId: req.body.roleId,
                    action: req.body.action,
                    attribute: req.body.attribute,
                    allow: req.body.allow,
                    allowIfManager: req.body.allowIfManager,
                    allowIfOwner: req.body.allowIfOwner
                }
                req.data = permission;
                await permissionQuery.create(req);
                resolve({status: "success", message: strings.permissionCreatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Get all permissions for a single role
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.roleId - the roleId to filter by
    * @return { Array<Permission> } an array of permissions
    */
    public async getByRoleId(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["roleId"]);
                req.data = {roleId: req.body.roleId}
                const permissions = await permissionQuery.getByRole(req);
                resolve({status: "success", data: permissions});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                let roleId: string = "*";
        		let orderBy: string = "ASC";
        		let orderByField: string = "attribute";
        		let queryString: string;
    
        		if(req.body.opts) {
                    if(req.body.opts.roleId !== undefined) {roleId = req.body.opts.roleId}
        			if(req.body.opts.orderBy != undefined) {orderBy = req.body.opts.orderBy};
        			if(req.body.opts.orderByField != undefined) {orderByField = req.body.opts.orderByField};  
        		}
    
        		queryString = `select * from permissions where roleId like "${roleId}" order by ${orderByField} ${orderBy};`;
    
        		const permissions = await permissionQuery.getCustom(queryString);
        		resolve({status: "success", data: permissions});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary update a permission
    * @param { ExpressRequest } req - default request object
    * @param { string } req.body.permissionId - permission Id to update
    * @return strings.permissionUpdatedSuccess
    */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["permissionId"]);
                req.data = {permissionId: req.body.udprn};
                const existing = await permissionQuery.getById(req);
                let updated: any = {};
                for(let [key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        updated[key] = req.body[key];
                    }
                }
                if(Object.keys(updated).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }
                req.data = {
                    update: updated,
                    permissionId: req.body.permissionId
                }
                permissionQuery.update(req);
                resolve({status: "suceess", message: strings.permissionUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

}

export default PermissionClass;