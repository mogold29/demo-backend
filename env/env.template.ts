export const env = {
    // application props
    applicationName: "Demo backend", 
    isProd: false,
    // db props
    dbUser: "",
    dbPassword: "",
    database: "demoBackendX1",
    dbPort: 3306,
    host: "localhost",
    // express props
    port: 9342,
    // authentication props
    rounds: 10,
    // Ideal postcode tokens
    idealPostcodesApiKey: "",
    idealPostcodeClientToken: "",
}

export default env;