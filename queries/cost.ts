import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { Cost } from "../models/cost";

/**
 * @class
 * @classdesc Query class to manage cost queries
 * @author Mo Gold
 */
export class CostQuery {

	/**
	 * @public
	 * @async
	 * @summary Create a cost
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const create = await query("insert into costs set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Create bulk costs
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<Cost> } req.data.costs - an array of costs
	 * @returns an empty promise
	 */
    public async createBulk(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
                const create = await query("insert into costs (jobId, userId, description, cost, category, dateSubmitted, costId) values ?;", req.data.costs);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	* @public
	* @async
	* @summary Get a cost by costId
	* @param { ExpressRequest } req - the defaut request obejct
	* @return an array with one cost
	*/
	public async getById(req: ExpressRequest) {
		return new Promise<Array<Cost>>(async(resolve, reject) => {
			try {
				const get = await query(`select costs.*, created.firstName as firstname, created.lastName as lastname, approved.firstName as approvedFirst, approved.lastName as approvedLast from costs join users as created on costs.userId = created.userId left join users as approved on costs.approveRejectedByUser = approved.userId where costs.costId = "${req.data.costId}";`, null);
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}
	/**
	 * @public
	 * @async
	 * @summary Get all costs of a job
	 * @param { ExpressRequest } req - the default request object
	 * @param { String } req.data.jobId - a jobId to filter by
	 * @returns { Array<Cost> } an array of costs, the result can
	 * be an empty array
	 */
    public async getByJob(req: ExpressRequest) {
        return new Promise<Array<Cost>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select costs.*, created.firstName as firstname, created.lastName as lastname, approved.firstName as approvedFirst, approved.lastName as approvedLast from costs join users as created on costs.userId = created.userId left join users as approved on costs.approveRejectedByUser = approved.userId where costs.jobId = "${req.data.jobId}";`, null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Get all costs by job and user
	 * @param { ExpressRequest } req - the default request object
	 * @returns { Array<Cost> } an array of costs, the result can be an empty array 
	 */
    public async getByJobAndUser(req: ExpressRequest) {
        return new Promise<Array<Cost>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select costs.*, created.firstName as firstname, created.lastName as lastname, approved.firstName as approvedFirst, approved.lastName as approvedLast from costs join users as created on costs.userId = created.userId left join users as approved on costs.approveRejectedByUser = approved.userId where costs.jobId = "${req.data.jobId}" and costs.userId = "${req.data.userId}";`, null, {canReturnZero: true, canReturnMultiple: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
	}
	
	/**
	 * @public
	 * @async
	 * @summary Approve or reject costs
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
	public async approveOrReject(req: ExpressRequest) {
		return new Promise(async(resolve, reject) => {
			try {
				await query(`update costs set ? where costId = "${req.data.costId}";`, req.data.update, {isUpdate: true});
				resolve()
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Approve or reject all costs of job
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async approveOrRejectByJob(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update costs set ? where jobId = "${req.data.jobId}" and approved = null and rejected = null;`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Approve all costs of a job submitted by a single user
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise 
	 */
    public async approveOrRejectByJobAndUser(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update costs set ? where jobId = "${req.data.jobId}" and userId = "${req.data.userId} and approved = null and rejected = null";`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Update a cost
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise 
	 */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update costs set ?  where costId = "${req.data.costId}";`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Delete a cost
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async delete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const deleteQuery = await query(`delete from costs where costId = "${req.data.costId}";`, null, {isDelete: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default CostQuery;