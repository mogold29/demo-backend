import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { File } from "../models/file";
import { FileClass } from "../logic/file";

const fileClass = new FileClass();


routes.post("/file/upload/:type/:id", async(req: ExpressRequest, res: any) => {
	try {
		const functionName = await fileClass.create(req);
		Responses.success(res, functionName);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/file/get", async(req: ExpressRequest, res: any) => {
    try {
		await Permission(req, "get", "files", req.body.fileId);
    	const get = await fileClass.get(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/file/download", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "download", "files");
		const download = await fileClass.download(req);
		Responses.file(res, download);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/file/delete", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "delete", "files");
		const deleteFile = await fileClass.delete(req);
		Responses.success(res, deleteFile);
	} catch(error) {
		Responses.error(res, error);
	}
});

export default routes;

