import { Response } from "express";
/**
 * @function successResponse
 * @summary Function to send a response with a file
 * @param res {object} - default express response object
 * @param data {any} - an error message, could be anything includng strings, functions, objects & numbers
 * @example responses.success(res, `error message`);
 */
export default function(res: Response, data?: any) {
    if(!data) {
        res.status(200).send({data: "Success"});
    } else {
        res.status(200).json({data: data});
    }
}