import { ExpressRequest } from "../models/express";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { AssignmentClass } from  "../logic/assignment";

const assignmentClass = new AssignmentClass();

routes.post("/assignment/recommend", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "recommend", "assignment");
		const recommend = await assignmentClass.recommend();
		Responses.success(res, recommend);
	} catch(error) {
		Responses.error(res, error);
	}
});

export default routes;