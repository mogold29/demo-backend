import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { LoginClass } from "../logic/auth/login";
const loginClass = new LoginClass();

routes.post("/auth/login", async(req: ExpressRequest, res: any) => {
    try {
    	const login = await loginClass.login(req);
    	Responses.success(res, login);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;