export interface Permission {
    [key: string]: any,
    roleId: string,
    permissionId: string,
    action: string,
    attribute: string,
    allow: boolean,
    allowIfOwner: boolean,
    allowIfManager: boolean,
    requireId: boolean
}