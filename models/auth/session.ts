export interface Session {
    userId: string,
    sessionToken: string,
    sessionExpiry: Date,
    sessionIp: string,
    sessionDeviceId: string,
    sessionDateCreated: Date,
    ownerId: string
};