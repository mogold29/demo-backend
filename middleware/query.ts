import * as mysql from "mysql";
import env from "../env/env";
import { resolve } from "path";
import { openSync } from "fs";

const connection = mysql.createConnection({
    host: env.host,
    database: env.database,
    user: env.dbUser,
    password: env.dbPassword
});

connection.connect();

/**
 * 
 * @function query
 * @description Run a MySql query
 * @param {string} query - a valid mysql query string
 * @param {object | null} data - a data object to insert/update or the keyword null
 * @param {Options} [opts] - either an empty object or valid selections of the Options interface
 * @param {boolean} [opts.isBulk] - is the query operation using bulk data such as an array of data
 * @param {boolean} [opts.canReturnMultiple] - can the query return multiple results
 * @param {boolean} [opts.isDelete] - is the query perfoming a delete opration
 * @param {booelan} [opts.canReturnZero] - can the query return zero results
 * @param {boolean} [opts.isUpdate] - is the query performing an update operation
 * @example query('select * from table', null, {}).then(response => {}).catch(error => {});
 * @example query('insert into table set ?', data, {}).then(response => {}).catch(error => {});
 * @example query('insert into table values ?', data, {isBulk: true}).then(response => {}).catch(error => {});
 * @returns {JSON} a json object of the result
 */

export default function(query: string, data: any, opts?: Options) {
    // initlize default options if undefined
    if(!opts) {opts = {}}
    if(!opts.canReturnMultiple) {opts.canReturnMultiple = false};
    if(!opts.isBulk) {opts.isBulk = false};
    if(!opts.isDelete) {opts.isDelete = false};
    if(!opts.isUpdate) {opts.isUpdate = false};
    if(!opts.canReturnZero) {opts.canReturnZero = false};

    return new Promise<Array<any>>((resolve, reject) => {
        console.log(query);
        
        connection.query(query, !data ? null : data,(err: any, responseD: any, fields: any) => {
            if(err) {
                let message;
                switch(err.errno) {
                    case 1022:
                        message = "😵 You have tried to use an existing value in a field which must be unique";
                    case 1027:
                        message = "Something has gone wrong on my end, i\'ve notified my master";
                    case 1032:
                        message = "I cant find that record 😔, im trying my utmost best to figure this out, i promise.";
                    case 1036:
                        message = "Hmm... 🤔 that\'s weird, im sure i was allowed to do that action, dont worry i just alerted my master about this 🤞";
                    case 1037:
                        message = "🤕 Ouch thats my bad, im restarting and please try doing this again in about 30 seconds";
                    case 1040:
                        message = "Oops i bit off more than i can chew, please give me a minute and try again";
                    case 1043:
                        message = "😒 nope i dont tust the situation, something terriblly wrong is happening please help me in alerting my master!";
                    case 1044:
                        message = "🔒 i tried the wrong keys, please hang tight while i get the correct keys";
                    case 1045:
                        message = "🔒 i tried the wrong keys, please hang tight while i get the correct keys";
                    case 1046:
                        message = "🔒 i tried the wrong keys, please hang tight while i get the correct keys";
                    case 1047:
                        message = "🙃 not sure what i was trying to do, that instruction makes no sense at all, please try again";
                    case 1048:
                        message = "🙃 not sure what i was trying to do, that instruction makes no sense at all, please try again";
                    case 1049:
                        message = "🙃 not sure what i was trying to do, that instruction makes no sense at all, please try again";
                    case 1054:
                        message = "🙃 not sure what i was trying to do, that instruction makes no sense at all, please try again";
                    case 1062:
                        message = "😵 You have tried to use an existing value in a filed which must be unique";
                }
                err["message"] = message || "";
                reject(err);
            }
            if(responseD == "undefined" || responseD == undefined || typeof responseD == "undefined" || !responseD) {responseD = {message: "Query error: undefined"}};
            
            const response = JSON.parse(JSON.stringify(responseD));

            // insert statements do not return values so we resolve witht the response
            if(data) {
                resolve(response);
            }

            // if isUpdate is true than make sure that the result has changed at least 1 row
            if(opts.isUpdate && response.affectedRows >= 1) {
                resolve(response);
            }

            if(opts.isDelete && response.affectedRows >= 1) {
                resolve(response);
            }

            // opts.canReturnZero should always be wrapped within a query that checks that the record actully exists and only the wrapped query calculation can return zero
            if(opts.canReturnZero) {
                resolve(response);
            }

            // if canReturnMultiple is true check that result length is greater than 0 and return the response otherwise reject
            if(opts.canReturnMultiple) {
                if(response.length > 0) {
                    resolve(response);
                } else {
                    reject("Invalid");
                }
            } else 
            // if canReturnMultiple is false the result should only have 1 object in the array anyhting else rejects
            {
                if(response.length != 1) {
                    reject("Invalid");
                } else {
                    resolve(response);
                }
            }
            reject("Invalid Query")
        });
    });
}

interface Options {
    isBulk?: boolean,
    canReturnMultiple?: boolean,
    isDelete?: boolean,
    isUpdate?: boolean,
    canReturnZero?: boolean
};