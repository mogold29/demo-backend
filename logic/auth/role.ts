import { ExpressRequest } from "../../models/express";
import { Response } from "../../models/response";
import { uuid } from "uuidv4";
import strings from "../../strings";
import requestCheck from "../../middleware/requestCheck";
import bulkRequestCheck from "../../middleware/bulkRequestCheck";
import { Role } from "../../models/auth/role";

import { RoleQuery } from "../../queries/auth/role";
const roleQuery = new RoleQuery();

export class RoleClass {

    /**
    * @public
    * @async
    * @summary Create a new role
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.roleName - the name of the role
    * @param { string } [req.body.roleDescription] - a short description of the role
    * @return success message if successfull
    */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["roleName"]);
                
                const role: Role = {
                    roleName: req.body.roleName,
                    roleDescription: req.body.roleDescription || "",
                    roleId: uuid()
                }

                req.data = role;
                await roleQuery.create(req);
                resolve({status: "success", message: strings.roleCreateSuccessfully});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Get all roles
    * @return an array of roles
    */
    public async get() {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const roles = await roleQuery.get();
                resolve({status: "success", data: roles});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Update a role
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.roleId - the roleId to update
    * @param { string } [req.body.roleName] - the updated role name
    * @param { string } [req.body.roleDescription] - the updated role description
    * @return success message if update is successful itherwise an error
    */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["roleId"]);
                req.data = {
                    roleId: req.body.roleId
                }
                const existing = await roleQuery.getSingleRole(req);

                let updated: any = {};
                for(let [key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        updated[key] = req.body[key];
                    }
                }
                if(Object.keys(updated).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }
                req.data = {
                    update: updated,
                    roleId: req.body.roleId
                }

                await roleQuery.update(req);
                resolve({status: "success", message: strings.roleUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default RoleClass;