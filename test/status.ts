import { assert, expect} from "chai";
import "mocha";

import { Status } from "../models/status";
import { StatusClass } from "../logic/status";
const statusClass = new StatusClass();

describe("Status tests", async() => {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        }
    };
    let res: any = {};

    describe("Get status", async() => {
        it("should get status successfully", async() => {
            const get = await statusClass.get();
            assert.isArray(get.data.status);
        });
    });
});