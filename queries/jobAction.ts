import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { JobAction } from "../models/jobAction";

/**
 * @class
 * @author Mo Gold
 * @classdesc JobActionQuery is the class for job actions
 */
export class JobActionQuery {

	/**
	 * @public
	 * @async
	 * @summary create a new job action
	 * @param { ExpressRequest} req - the efault requets object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
                const create = await query("insert into jobActions set ?;", req.data);
        		resolve();
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	* @public
	* @async
	* @summary Create bulk job action
	* @param { ExpressRequest } req - the default request object
	* @return an empty promise
	*/
	public async bulkCreate(req: ExpressRequest) {
		return new Promise(async(resolve, reject) => {
			try {
				const create = await query("insert into jobActions (actionName, jobId, isSuccess, notes, timeOfAction, userId, actionId, ownerId, actionType) values ?", req.data.actions, {isBulk: true});
				resolve()
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Get job actions filtered by job
	 * @param { ExpressRequest} req - the efault requets object
	 * @returns { Array<JobAction> } an array of job actions, can be zero length
	 */
    public async get(req: ExpressRequest) {
        return new Promise<Array<JobAction>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select jobActions.*, users.firstName, users.lastName from jobActions join users on users.userId = jobactions.ownerId where jobActions.jobId = "${req.data.jobId}" order by timeOfAction DESC;`, null, {canReturnZero: true, canReturnMultiple: true});
        		resolve(get);
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary delete a job action
	 * @param { ExpressRequest} req - the efault requets object
	 * @returns an empty promise
	 */
    public async delete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const deleteQuery = await query(`delete from jobActions where actionId = "${req.data.actionId}";`, null, {isDelete: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Update an action
	 * @param { ExpressRequest} req - the efault requets object
	 * @returns an empty promise
	 */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const update = await query(`update jobActions set ? where actionId = "${req.data.actionId}";`, req.data.update)
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default JobActionQuery;