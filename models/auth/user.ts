export interface User {
    [key: string]: any,
    firstName: string,
    lastName: string,
    emailAddress: string,
    password: string,
    phoneNumber: string,
    tfaEnabled: boolean,
    forceChangePassword: boolean,
    userId: string,
    roleId: string,
    ownerId: string,
    userManager: string,
    tfaSecret?: string,
    shortId?: number,
    isContractor?: boolean
}