import express from "express";
const routes = express.Router();
import session from "./middleware/session";
import  * as response from "./responses/reponse";
import { ExpressRequest } from "./models/express";
import { env } from "./env/env";

import customerRoutes from "./routes/customer";
import jobPriorityRoutes from "./routes/jobPriority";
import addressRoutes from "./routes/address";
import contactRoutes from "./routes/contactPepole";
import fileRoutes from "./routes/file";
import actionRoutes from "./routes/jobAction";
import costRoutes from "./routes/cost";
import statusRoutes from "./routes/status";
import assignmentRoutes from "./routes/assignment";
import permissionRoutes from "./routes/permission";
import jobRoutes from "./routes/job";
import loginRoutes from "./routes/login";
import authRoutes from "./routes/auth";
import userRoutes from "./routes/user";
import roleRoutes from "./routes/role";
import noteRoutes from "./routes/notes";

import bodyParser from "body-parser";
routes.use(bodyParser.urlencoded({ extended: false }))
routes.use(bodyParser.json())

const cookieParser = require("cookie-parser");
routes.use(cookieParser());

routes.use((req, res, next) => {
    if(req.query && req.body == undefined) {
        req.body = req.query;
    }
    next();
})

routes.use(function(req, res, next) {
    if (!env.isProd) {
        res.header("Access-Control-Allow-Origin", "http://localhost:4200"); 
    }
    
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, authorization, deviceId");
    next();
});

routes.use((req: ExpressRequest, res, next) => {
    if (req.headers.deviceid) {
        req["authenticate"] = {}
        req.authenticate.deviceId = req.headers.deviceid.toString();
    }
    next()
});

routes.use(async(req: ExpressRequest, res, next) => {
    try {
        await session(req, res, next);
        next();
    } catch(error) {
        response.error(res, error);
    }
});


routes.use(customerRoutes);
routes.use(jobPriorityRoutes);
routes.use(addressRoutes);
routes.use(contactRoutes);
routes.use(fileRoutes);
routes.use(actionRoutes);
routes.use(costRoutes);
routes.use(statusRoutes);
routes.use(assignmentRoutes);
routes.use(permissionRoutes);
routes.use(jobRoutes);
routes.use(authRoutes);
routes.use(loginRoutes);
routes.use(userRoutes);
routes.use(roleRoutes);
routes.use(noteRoutes);

export default {routes}