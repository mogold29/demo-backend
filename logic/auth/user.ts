import bcrypt from "bcrypt";
import { uuid } from "uuidv4";
import env from "../../env/env";
import strings from "../../strings";
import { SessionClass as sc } from "./session";
import { User } from "../../models//auth/user";
import { Response } from "../../models/response";
import { ExpressRequest } from "../../models/express";
import { isManager } from "../../middleware/isManager";
import requestCheck from "../../middleware/requestCheck";
import { UserQuery as uq } from "../../queries/auth/user";
import { SignupToken } from "../../models/auth/signupToken";
import { PreviousPassword } from "../../models/auth/previousPassword";

const UserQuery = new uq();
const SessionClass = new sc();

/**
 * @class
 * @classdesc User class manages logic for users
 * @author Mo Gold
 */
export class UserClass {

    /**
     * @public
     * @async
     * @summary Create a new user
     * @description - Create a new user, when creating a new
     * user a password isnt set this happenes at a later stage
     * @param {ExpressRequest} req - the default request object
     * @param {string} req.body.firstName - the users first name
     * @param {string} req.body.lastName - the users last name
     * @param {string} req.body.emailAddress - the users email address, must be unique
     * @param {string} req.body.phoneNumber - the users phone number
     * @param {string} req.body.roleId - a role Id for this user
     * @param {string} req.body.userManager - a user Id of the new users manager
     * @returns {Response} success or error with a message
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                const check = await requestCheck(req, ["firstName", "lastName", "emailAddress", "phoneNumber", "roleId"]);

                const outsideUserId = uuid();

                const user: User = {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    emailAddress: req.body.emailAddress,
                    phoneNumber: req.body.phoneNumber,
                    tfaEnabled: false,
                    forceChangePassword: true,
                    userId: outsideUserId,
                    roleId: req.body.roleId,
                    ownerId: outsideUserId,
                    userManager: req.body.userManager || "",
                    password: bcrypt.hashSync(req.body.password, env.rounds)
                }

                req.data = user
                await UserQuery.create(req);
                
                resolve({status: "success", message: "your account has been created successfully, please check your email for further instructions"});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @async
     * @public
     * @summary Update a user
     * @description Update a user by providing a User object with
     * modified properties
     * @param {ExpressRequest} req - the default request object
     * @param {string} req.body.userId - the user Id of the user to update
     * @returns {Response} a success message or an error message
     */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                await requestCheck(req, ["userId"]);

                req.data = {
                    userId: req.body.userId
                }

                const existingUserDetailsQuery = await UserQuery.getSingleUser(req);
                const existingUser = existingUserDetailsQuery[0];
                let userUpdate: any = {};

                for(let [key, val] of Object.entries(req.body)) {
                    if(req.body[key] !== existingUser[key]) {
                        userUpdate[key] = req.body[key];
                    }
                }

                req.data = {
                    userId: req.body.userId,
                    update: userUpdate
                }

                await UserQuery.updateUser(req);
                resolve({status: "success", message: "User successfully updated"});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @async
     * @public
     * @summary Get user details
     * @description Get user details with selected user Id
     * @param {ExpressRequest} req - the default request object
     * @param {string} req.body.userId - the user Id to get
     * @returns {Response} A response object with a data object containing
     * an array of one user 
     */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                requestCheck(req, ["userId"]);
                const users = await UserQuery.getSingleUser(req);
                delete users[0].password;
                resolve({status: "success", data: users});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
    * @public
    * @async
    * @summary get a user by their email address
    * @param { ExpressRequest } req - defualt request obejct
    * @param { string } req.body.emailAddress - the users email address
    * @return an array with a single user
    */
    public async getByEmail(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["emailAddress"]);
                req.data = {emailAddress: req.body.emailAddress}
                const get = await UserQuery.getByEmail(req);
                resolve({status: "success", data: get});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Get all users
    * @param { ExpressRequest } req - the default request object
    * @return { Array<User> } an array of users in the data object
    */
    public async getAll(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const users: any = await UserQuery.getAll(req);
                resolve({status: "success", data: users});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    public async getCustom(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                let limit: number = 0;
                let offset: number = 10;
                let orderBy: string = "ASC";
                let orderByField: string = "lastName";
                let queryString: string;
                let countQueryString: string;

                if(req.body.opts) {
                    if(req.body.opts.offset != undefined) {offset = req.body.opts.offset};
                    if(req.body.opts.limit != undefined) {limit = req.body.opts.limit};
                    if(req.body.opts.orderBy != undefined) {orderBy = req.body.opts.orderBy};
                    if(req.body.opts.orderByField != undefined) {orderByField = req.body.opts.orderByField}; 
                }

                queryString = `select * from users order by ${orderByField} ${orderBy} limit ${limit}, ${offset};`;
                countQueryString = `select count(firstName) as 'count' from users;`;

                if(req.body.opts) {
                    if(req.body.opts.field != undefined) {
                        queryString = `select * from users where ${req.body.opts.field} like "%${req.body.opts.searchterm}%" order by ${orderByField} ${orderBy} limit ${limit}, ${offset};`;
                        countQueryString = `select count(firstName) as 'count' from users where ${req.body.opts.field} like "%${req.body.opts.searchterm}%";`
                    }
                }

                const usersQuery = await UserQuery.getCustom(queryString);
                const countQuery = await UserQuery.getCountOfUsers(countQueryString);

                resolve({status: "success", data: {usersQuery, countQuery}});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
    * @public
    * @async
    * @summary Activate a user
    * @param { ExpressRequest } req - the default request object
    * @param { stirng } req.body.userId - the user to activate
    * @return strings.userActivatedSuccess
    */
    public async activate(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["userId"]);
                //await isManager(req, req.body.userId);
                await UserQuery.activateUser(req);
                resolve({status: "success", message: strings.userActivatedSuccess})
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Deactivate a user
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async deactivate(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["userId"]);
                //await isManager(req, req.body.userId);
                await UserQuery.deactivateUser(req);
                resolve({status: "success", message: strings.userDeactivatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

}