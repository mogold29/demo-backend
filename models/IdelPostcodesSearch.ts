export interface IdealPostcodesSearch {
    "result": {
        "hits": Array<Hit>
    },
    "code": number,
    "message": string
}

export interface Hit {
    "suggestion": string,
    "urls": {
        "udprn": string
    },
    "udprn": number
}

export default IdealPostcodesSearch;