import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import bulkRequestCheck from "../middleware/bulkRequestCheck";
import { Cost } from "../models/cost";
import { CostQuery } from "../queries/cost";

const costQuery = new CostQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc CostClass manages all logic relating to costs
 */
export class CostClass {

    /**
     * @public
     * @async
     * @summary Create a new cost
     * @param { ExpressRequest } req - the default request object
     * @param { string } jobId - the jobId to which the cost is linked
     * @param { string } description - a description for the cost
     * @param { number } cost - a float number, the charge paid for the cost
     * @param { string } category - a category for the cost
     * @returns { Response } strings.costCreateSuccess
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId", "description", "cost", "category"]);
                const cost: Cost = {
                    jobId: req.body.jobId,
                    userId: req.authenticate.userId,
                    description: req.body.description,
                    cost: req.body.cost,
                    category: req.body.category,
                    dateSubmitted: new Date(),
                    costId: uuid(),
                    ownerId: req.authenticate.userId
                }
                req.data = cost;
                await costQuery.create(req);
                resolve({status: "success", message: strings.costCreateSuccess});
        	} catch(error) {
                console.log(error);
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Create bulk costs
     * @param { ExpressRequest }  req - the default request object
     * @param { Array<Cost> } req.body.costs - an array of costs
     * @param { stirng } req.body.costs.jobId - the jobId to which the cost is linked
     * @param { string } req.body.costs.description - a description for the cost
     * @param { number } req.body.costs.cost - a float number, the charge paid for the cost
     * @param { string } req.body.costs.category - a category for the cost
     * @returns { Response } strings.bulkCostCreateSuccess
     */
    public async createBulk(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await bulkRequestCheck(req, ["jobId", "description", "cost", "category"], "costs");
                const bulkCosts: any = [[]];
                req.body.costs.forEach((cost: Cost) => {
                    const costX = [
                        cost.jobId,
                        req.authenticate.userId,
                        cost.description,
                        cost.cost,
                        cost.category,
                        new Date(),
                        uuid()
                    ];
                    bulkCosts[0].push(costX);
                });
                req.data = {costs: bulkCosts}
                const create = await costQuery.createBulk(req);
                resolve({status: "success", message: strings.bulkCostCreateSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Get a single cost
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.costId - the cost Id to get
    * @return { Response } an array with a single cost in promise.data
    */
    public async getSingleCost(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["costId"]);
                req.data = {costId: req.body.costId}
                const cost = await costQuery.getById(req);
                resolve({status: "success", data: cost});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get all costs of a job
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.jobId - the jobId to filter by
     * @returns { Response } an array of costs in the res.data 
     */
    public async getByJob(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId"]);
                req.data = {jobId: req.body.jobId}
                const costs = await costQuery.getByJob(req);
                resolve({status: "success", data: {costs}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get costs filter by job and user
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.jobId - the jobId to filter by
     * @param { string } req.body.userId - the userId to filter by
     * @returns { Response } an array of costs in the res.data 
     */
    public async getByJobAndUser(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId", "userId"]);
                req.data = {
                    jobId: req.body.jobId,
                    userId: req.body.userId
                }
                const costs = await costQuery.getByJobAndUser(req);
                resolve({status: "success", data: {costs}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Update a costs
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.costId - the costId to update
     * @returns { Response }  strings.costUpdatedSuccess
     */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["costId"]);
                req.data = {costId: req.body.costId}
                const existing = await costQuery.getById(req);
                let cost: any = {};
                for(let [key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        cost[key] = req.body[key]
                    }
                }
                if(Object.keys(cost).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }
                req.data = {
                    costId: req.body.costId,
                    update: cost
                }
                const update = await costQuery.update(req);
                resolve({status: "success", message: strings.costUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Approve a single cost
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.costId - the costId to approve
     * @returns { Response } strings.costApproved
     */
    public async approve(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["costId"]);
                req.data = {
                    costId: req.body.costId,
                    update: {
                        dateApprovedRejected: new Date(),
                        approveRejectedByUser: req.authenticate.userId,
                        approved: true
                    }
                }
                await costQuery.approveOrReject(req);
                resolve({status: "success", message: strings.costApproved});
        	} catch(error) {
                console.log(error);
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary reject a single cost
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.costId - the costId to reject
     * @returns { Response } strings.costsRejected
     */
    public async reject(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["costId"]);
                req.data = {
                    costId: req.body.costId,
                    update: {
                        dateApprovedRejected: new Date(),
                        approveRejectedByUser: req.authenticate.userId,
                        rejected: true
                    }
                }
                const reject = await costQuery.approveOrReject(req);
                this.notify(req, "rejected");
                resolve({status: "success", message: strings.costsRejected});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Reject all costs of a job
     * @param { ExpressRequest } req - the default request object
     * @param { stirng } req.body.jobId - the jobId to reject all costs of
     * @returns { Response } strings.costsRejected
     */
    public async rejectByJob(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId"]);
                req.data = {
                    jobId: req.body.jobId,
                    update: {
                        dateApprovedRejected: new Date(),
                        approveRejectedByUser: req.authenticate.userId,
                        rejected: true
                    }
                }
                const update = await costQuery.approveOrRejectByJob(req);
                const notify = await this.notify(req, "rejected");
                resolve({status: "success", message: strings.costsRejected});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Approve all costs of a job
     * @param { ExpressRequest } req - the default request object
     * @param { stirng } req.body.jobId - the jobId to approve all costs of
     * @returns { Response } strings.costApproved
     */
    public async approveByJob(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId"]);
                req.data = {
                    jobId: req.body.jobId,
                    update: {
                        dateApprovedRejected: new Date(),
                        approveRejectedByUser: req.authenticate.userId,
                        approved: true
                    }
                }
                const update = await costQuery.approveOrRejectByJob(req);
                const notify = await this.notify(req, "approved");
                resolve({status: "success", message: strings.costApproved});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Approve all costs of a job submitted by a single user
     * @param { ExpressRequest } req - the default request object
     * @param { stirng } req.body.jobId - the jobId to approve all costs of
     * @param { string } req.body.userId - the userId to approve by
     * @returns { Response } strings.costApproved
     */
    public async approveByJobAndUser(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId", "userId"]);
                req.data = {
                    jobId: req.body.jobId,
                    userId: req.body.userId,
                    update: {
                        dateApprovedRejected: new Date(),
                        approveRejectedByUser: req.authenticate.userId,
                        approved: true
                    }
                }
                const update = await costQuery.approveOrRejectByJobAndUser(req);
                const notify = await this.notify(req, "approved");
                resolve({status: "success", message: strings.costApproved});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Reject all costs of a job
     * @param { ExpressRequest } req - the default request object
     * @param { stirng } req.body.jobId - the jobId to reject all costs of
     * @param { string } req.body.userId - the userId to reject by
     * @returns { Response } strings.costsRejected
     */
    public async rejectByJobAndUser(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["jobId", "userId"]);
                req.data = {
                    jobId: req.body.jobId,
                    userId: req.body.userId,
                    update: {
                        dateApprovedRejected: new Date(),
                        approveRejectedByUser: req.authenticate.userId,
                        rejected: true
                    }
                }
                const update = await costQuery.approveOrRejectByJobAndUser(req);
                const notify = await this.notify(req, "rejected")
                resolve({status: "success", message: strings.costsRejected});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @param
     * @async
     * @summary Delete a cost
     * @param { ExpressRequest } req - the default requets object
     * @param { string } req.body.costId - the costId to delete
     * @returns { Response } strings.costDeletedSuccess
     */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["costId"]);
                req.data = {costId: req.body.costId}
                const deleteQuery = await costQuery.delete(req);
                resolve({status: "success", message: strings.costDeletedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @private
     * @async
     * @summary Helper function to notify users of rejected costs
     * this submits the notification to the system notification stack
     * @param { ExpressRequest } req - the default request object
     * @param { string } reason - the reason of the notification
     * @returns an empty promise 
     * @todo create functinality
     */
    private async notify(req: ExpressRequest, reason: string) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                resolve();
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default CostClass;