import query from "../../middleware/query";
import { Session } from "../../models/session";
import { OldSession } from "../../models/auth/oldSession";
import { ExpressRequest } from "../../models/express";
import { UnionSession } from "../../models/auth/unionSessions";

/**
 * @class
 * @description queries for managing sessions
 */
export class SessionQuery {

    /**
     * @public
     * @async
     * @description create a new session
     * @param {ExpressRequest} req - the default request object
     * @param {Session} req.data - an object with the session data
     * @returns an empty promise
     */
    public async create (req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const createQuery = await query("insert into sessions set ?", req.data,  {});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @description extend a session by providing a new expiry datetime
     * @param {ExpressRequest} req - the default request object
     * @param {string} req.authenticate.sessionToken - the pre populate sessionToken
     * @param {object} req.data.update - an update object with the proeprties and values to update
     * @returns an empty promise
     */
    public async extend(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const updateQuery = await query("update sessions set ? where seesionToken = '" + req.authenticate.sessionToken + "';", req.data.update, {});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @description end a single session and copy into the oldSessions table
     * @param {object} req - the default request object
     * @param {string} req.authenticate.sessionToken - the pre populated sessionToken
     * @returns an empty promise
     */
    public async end(req: ExpressRequest) {
        return new Promise<Array<Session>>(async(resolve, reject) => {
            try {
                const copyQuery: any = await query("select * from sessions where sessionToken = '" + req.authenticate.sessionToken + "';", null, {});
                
                copyQuery[0]["sessionDateEnded"] = new Date();
                copyQuery[0]["sessionExpiry"] = new Date(copyQuery[0].sessionExpiry);
                copyQuery[0]["sessionDateCreated"] = new Date(copyQuery[0].sessionDateCreated);

                await query("insert into oldSessions set ?", copyQuery[0], {});
                await query("delete from sessions where sessionToken = '" + req.authenticate.sessionToken + "';", null, {isDelete: true});

                resolve();
            } catch(error) {
                console.log(error);
                reject(error);
            }
        });
    }

    /**
     * @public
     * @async
     * @description gets all sessions for a user, active and ended sessions
     * @param {object} req - the default request object
     * @param {string} req.data.userId - the userId of which to get sessions for
     * @returns {Array<Session>} returns an array of sessions
     */
    public async getAllSessions(req: ExpressRequest) {
        return new Promise<UnionSession>(async(resolve, reject) => {
            try {
                const currentSessions: any = await query("select * from sessions where userId = '" + req.data.userId + "';", null, {canReturnZero: true, canReturnMultiple: true});
                const oldSessions: any = await query("select * from oldSessions where userId = '" + req.data.userId + "';", null, {canReturnMultiple: true, canReturnZero: true});

                const formattedCurrentSessions: Array<Session> = currentSessions.map(this.addSessionStatus(currentSessions, "Active"));
                const formattedOldSessions: Array<OldSession> = oldSessions.map(this.addSessionStatus(oldSessions, "Ended"));

                resolve({
                    sessions: formattedCurrentSessions, 
                    oldSessions: formattedOldSessions
                });

            } catch(error) {
                reject(error);
            }
        })
    }

    /**
    * @public
    * @async
    * @summary Check if the current session is still active
    * default check is 90 days from session start
    * @param { ExpressRequest } req - the default request object
    * @return {Array<Session>} an array with one session if the session
    * is still active
    */
    public async isSessionActive(req: ExpressRequest) {
        return new Promise<Array<Session>>(async(resolve, reject) => {
        	try {
        		const session = await query(`select * from sessions where sessionToken = "${req.headers.authorization}" and date_add(sessionExpiry, interval 90 day) > curdate();`, null, {canReturnZero: true})
        		resolve(session)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
     * @public
     * @async
     * @description get a single session filtered by the sessionToken present on the req.authenticate object
     * @param req - the default request object
     * @param req.authenticate.sessionToken - the pre populated sessionToken
     * @returns {Array<Sesssion>} returns an array with a single session
     */
    public async get(req: ExpressRequest) {
        return new Promise<Array<Session>>(async(resolve, reject) => {
            try {
                const getQuery: any = await query("select * from sessions where sessionToken = '" + req.authenticate.sessionToken + "';", null, {canReturnZero: true});
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @private
     * @description add a session status to an array of sessions, either "Active" or "Ended"
     * @param {Session} session - a session object to modify, genrally taken from array.map
     * @param {string} status - a status string, either "Active" or "Ended"
     * @returns {Object} a modified session object with a status appended to the original object 
     */
    private async addSessionStatus(session: any, status: string) {
        const newSessonObject = session;
        newSessonObject["status"] = status;
        return newSessonObject;
    }
}