import { ExpressRequest } from "../models/express";
/** 
* @function bulkRequestCheck 
* @summary Checks if the req has a body and if the req.body conains required parameters compared to the fields param
* @param {Object} req - the default express request
* @param {Array} fields - a string array of required fields to check if they exist in the req.body 
**/

export default function(req: any, fields: Array<string>, bulkProperty: string) {
    
    return new Promise((resolve, reject) => {
        if(!req.body) {
            reject(new Error(null));
        }
        
        fields.forEach((field) => {
            console.log(`${req.body[bulkProperty]}`);
            req.body[bulkProperty].forEach((property: any, index: number) => {
                if(!property.hasOwnProperty(field)) {
                    reject(new Error(`Property ${field} is missing at position ${index}`));
                }
            })
        });

        resolve("good");
    })
};