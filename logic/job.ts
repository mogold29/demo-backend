const fs = require("fs");
const path = require("path");
import { uuid } from "uuidv4";
import strings from "../strings";
import { PdfClass } from "./pdf";
import { FileClass } from "./file";
import { UserClass } from "./auth/user";
import { JobQuery } from "../queries/job";
import { NotesClass } from "../logic/notes";
import { JobActionClass } from "./jobAction";
import location from "../middleware/location";
import { Response } from "../models/response";
import { Job, JobObject } from "../models/job";
import { JobAction } from "../models/jobAction";
import { StatusQuery } from "../queries/status";
import { JobPriorityClass } from "./jobPriority";
import { Assignment } from "../models/assignment";
import { ExpressRequest } from "../models/express";
import { CustomerQuery } from "../queries/customer";
import ContactPerson from "../models/contactPepole";
import jsDateFormat from "../middleware/jsDateFormat";
import requestCheck from "../middleware/requestCheck";
import JobPriorityQuery from "../queries/jobPriority";
import { AssignmentQuery } from "../queries/assignemnt";
import { ContactPepoleQuery } from "../queries/contactPepole";

const jobQuery = new JobQuery();
const fileCLass = new FileClass();
const assignmentQuery = new AssignmentQuery();
const statusQuery = new StatusQuery();
const userClass = new UserClass();
const notesClass = new NotesClass();
const jobActionClass = new JobActionClass();
const jobPriorityClass = new JobPriorityClass();
const contactPeopleQuery = new ContactPepoleQuery();
const jobPriorityQuery = new JobPriorityQuery();
const pdfClass = new PdfClass();
const customerQuery = new CustomerQuery();

const distancePreferance = 1000;

 /////// STATUS POSITIONS ///////
const deleteStatus = 10;
const cancelStatus = 30;
const onSiteStatus = 35;
const accessStatus = 30;
const rejectStatus = 20;
const createdStatus = 10;
const approvedStatus = 50;
const invoicedStatus = 60;
const onSiteCompleted = 40;
const roadCompletedStatus = 45;
const changeAssignmentStatus = 35;
const assignedNotBookedStatus = 20;
const createdAndBookedWithTenantStatus = 15;

/**
 * @class
 * @author Mo Gold
 * @classdesc JobClass manages all logic for jobs
 */
export class JobClass {
    
    /**
	 * @public
	 * @async
	 * @summary Create a new Job
	 * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId that the job is linked to
     * @param { string } req.body.customerName - the current customer name
     * @param { string } req.body.customerAddressLineOne - the current customers address line one
     * @param { string } req.body.customerPostcode - the current customers postcode
     * @param { string } req.body.customerCity - the current customers city
     * @param { string } req.body.addressId - the addressId the job is linked to
     * @param { string } req.body.category - the job category
     * @param { string } req.body.priorityId - the priorityId for the job
     * @param { string } req.body.tenantFirstName - the tenants first name
     * @param { string } req.body.tenantLastName - the tenants last name
     * @param { string } req.body.tenantPhoneNumberOne - the tenants phone number
     * @param { string } req.body.tenantEmailAddress - the tenants email address
     * @param { string } req.body.jobDescription - a short description of the job
     * @param { string } req.body.contactPersonId - the contactPersonId that requested the job
     * @param { string } [req.body.customerAddressLineTwo] - the second line of the customers address
     * @param { string } [req.body.customerAddressLineThree] - the third line of the customers address
     * @param { string } [req.body.tenantPhoneNumberTwo] - the second phone number for the tenant
     * @param { string } [req.body.tenantPhoneNumberThree] - the third phone number for the tenant
     * @param { string } [req.body.tenantPhoneNumberFour] - the fourth phone number for the tenant
     * @param { string } [req.body.accessDetails] - access details for the property
     * @param { string } [req.body.keyLocation] - the location of the key
     * @param { string } [req.body.jobNotes] - notes about the job
     * @param { number } [req.body.quote] - a quote provided for the job
	 * @returns { Response } strings.jobCreatedSuccessfully & jobId on the res.data
	 */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["customerId", "currentCustomerName", "currentCustomerAddressLineOne", "currentCustomerAddressPostcode", "currentCustomerAddressCity", "addressId", "category", "priorityId", "tenantFirstName", "tenantPhoneNumberOne", "tenantEmailAddress", "jobDescription", "contactPersonId"]);
                let job: any = {};
                // iterate over job interface object and if property is in req.body insert into new job object
                for(let [key, val] of Object.entries(JobObject)) {
                    if(req.body[key] != undefined) {
                        job[key] = req.body[key];
                        // else if property is a string and empty set property in new job object to ""
                    }
                }

                // set due date if undefined
                if(req.body.dueDate == undefined) {
                    // get default from db
                    const x = new Date()
                    x.setDate(x.getDate() + 3);
                    job.dueDate = x;
                }

                // set default job properties
                job.jobId = uuid();
                job.jobCreationDate = new Date();
                job.status = 10,
                req.data = job;
                job.ownerId = req.authenticate.userId;
                
                await jobQuery.create(req);
                
                // add job action 
                req.body["actionName"] = "Created the job";
                req.body["actionType"] = "Create"
                req.body["jobId"] = job.jobId;
                req.body["isSuccess"] = true;
                req.body["notes"] = "";
                await jobActionClass.create(req);

                // add notes
                if(req.body.jobNotes) {
                    req.body["jobId"] = job.jobId;
                    req.body["title"] = req.body.jobNotes.substring(0, 240);
                    req.body["content"] = req.body.jobNotes;
                    req.body["attribute"] = "job";
                    req.body["attributeId"] = job.jobId;
                    await notesClass.create(req);
                }

                // add files
                if(req.files) {
                    req.body["fileType"] = "customer";
                    req.body["fileOwner"] = job.jobId;
                    await fileCLass.create(req);
                }

                resolve({status: "success", data: {
                    jobId: job.jobId,
                    message: strings.jobCreatedSuccessfully
                }});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Get a single job by Id
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.jobId - the jobId to get
    * @return { Response } an array with one job & all statuses and any files related to the job in the res.data
    */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                req.data = {
                    jobId: req.body.jobId,
                    fileType: "job",
                    fileOwner: req.body.jobId
                }
                const job = await jobQuery.getById(req);
                const status = await statusQuery.get();
                const users = await userClass.getAll(req);
                const priorities = await (await jobPriorityClass.get()).data.priorities;
                const assignments = await (await assignmentQuery.getByJob(req));

                req.data["customerId"] = job[0].customerId;

                const customer = await (await customerQuery.getSingleById(req));
                
                req.body["fileOwner"] = req.body.jobId;
                req.body["fileType"] = "job";
                
                const files = await fileCLass.get(req);
                
                req.body["attribute"] = "job";
                req.body["attributeId"] = req.body.jobId;

                const notes = await notesClass.get(req);
                const actions = await jobActionClass.get(req);
                
                resolve({status: "success", data: {
                    job,
                    status,
                    files,
                    users,
                    notes,
                    actions,
                    priorities,
                    assignments,
                    customer
                }});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    public async getCustom(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
        		let limit: number = 0;
        		let offset: number = 10;
        		let orderBy: string = "ASC";
        		let orderByField: string = "jobCreationDate";
        		let queryString: string;
        		let countQueryString: string;
                let cancelled = "jobs.isCancelled like null";
    
        		if(req.body.opts) {
        			if(req.body.opts.offset != undefined) {offset = req.body.opts.offset};
        			if(req.body.opts.limit != undefined) {limit = req.body.opts.limit};
        			if(req.body.opts.orderBy != undefined) {orderBy = req.body.opts.orderBy};
        			if(req.body.opts.orderByField != undefined) {orderByField = req.body.opts.orderByField};  
        			if(req.body.opts.cancelled != undefined) {
                        req.body.opts.cancelled === true ? cancelled = "jobs.isCancelled is null or jobs.isCancelled = true" : cancelled = "jobs.isCancelled is null"
                    };
                }
                

        		queryString = `select jobs.*, jobPriorities.priorityName from jobs join jobPriorities on jobPriorities.priorityId = jobs.priorityId where ${cancelled} order by jobs.${orderByField} ${orderBy} limit ${limit}, ${offset};`;
        		countQueryString = `select count(jobId) as 'count' from jobs where ${cancelled};`;
    
        		if(req.body.opts) {
        			if(req.body.opts.field != undefined) {
        				queryString = `select jobs.*, jobPriorities.priorityName from jobs join jobPriorities on jobPriorities.priorityId = jobs.priorityId where jobs.${req.body.opts.field} like "%${req.body.opts.searchterm}%" and ${cancelled} order by jobs.${orderByField} ${orderBy} limit ${limit}, ${offset};`;
        				countQueryString = `select count(addressLineOne) as 'count' from jobs where ${req.body.opts.field} like "%${req.body.opts.searchterm}%" and ${cancelled};`
        			}
        		}
    
                const customQuery = await jobQuery.get(queryString);
        		const count = await jobQuery.get(countQueryString);
        		resolve({status: "success", data: {customQuery, count}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Update a job
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.jobId - the jobId to update
    * @return strings.jobUpdatedSuccess
    */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                req.data = {jobId: req.body.jobId}
                const existing = await jobQuery.getById(req);
                let updated: any = {};
                for(let [key, val] of Object.entries(existing[0])) {
                    if(existing[0][key] !== req.body[key] && req.body[key] !== undefined) {
                        updated[key] = req.body[key]
                    }
                }
                if(Object.keys(updated).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }
                req.data = {
                    jobId: req.body.jobId,
                    update: updated
                }
                await jobQuery.update(req);
                resolve({
                    status: "success", 
                    message: strings.jobUpdatedSuccess
                });
        	} catch(error) {
                console.log(error);
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary Update a job priority
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId to update
     * @param { string } req.body.priorityId - the priority to update to
	 * @returns { Response } the new priortity in the res.data
	 */
    public async updatePriority(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId", "priorityId"]);
                req.data = {
                    jobId: req.body.jobId,
                    update: {priorityId: req.body.priorityId}
                }
                const update = await jobQuery.update(req);
                resolve({
                    status: "success",
                    data: {
                        message: strings.jobUpdatedSuccess,
                        priorityId: req.body.priorityId
                    }
                });
        	} catch(error) {
        		reject(error);
        	};
        });
    }

   /**
	 * @public
	 * @async
	 * @summary Assing a job to a user
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId to assign
     * @param { string } req.body.userId - the userId t which the job is being assigned
	 * @returns { Response } the updated status, an object with user detail in the res.data
	 */
    public async assignJob(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId", "userId"]);
                req.data = {jobId: req.body.jobId}
                const existing = await jobQuery.getById(req);
                let updated: any = {};
                if(existing[0].status == createdAndBookedWithTenantStatus) {
                    updated["status"] = accessStatus;
                } else if(existing[0].status == createdStatus) {
                    updated["status"] = assignedNotBookedStatus
                } else {
                    resolve({status: "error", message: strings.assignmentWrongStatus});
                }
                req.data = {
                    jobId: req.body.jobId,
                    update: updated
                }
                await jobQuery.update(req);
                
                delete updated["status"];
                updated.jobId = req.body.jobId;
                updated.userId = req.body.userId,
                updated.dateAssigned = new Date(),
                updated.assignedByUser = req.authenticate.userId;
                req.data = {jobId: req.body.jobId, update: updated}
                await assignmentQuery.update(req);
                resolve({
                    status: "success",
                    message: strings.jobAssignedSuccess
                });
        	} catch(error) {
                console.log(error);
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Update multiple jobs to assigned status. this should only
    * be used when udating from another module and should not be exposed
    * to the api
    * @param { ExpressRequest } req - the default reequest obejct
    * @param { Array<object> } assignments - an array of assignments with 2 properties
    * @param { string } assignments.jobId - the jobId which has been assigned
    * @return a success status
    */
    public async assignMultiple(req: ExpressRequest, assignments: Array<any>) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
        		assignments.forEach(async (assignment) => {
                    req.data = {jobId: assignment.jobId}
                    const exisiting = await jobQuery.getById(req);
                    let update: any = {};
                    if(exisiting[0].status == createdAndBookedWithTenantStatus) {
                        update["status"] = accessStatus;
                    } else if(exisiting[0].status == createdStatus) {
                        update["status"] = assignedNotBookedStatus;
                    } else {
                        resolve({status: "error", message: strings.assignmentWrongStatus});
                    }
                    req.data = {
                        jobId: assignment.jobId,
                        update
                    }
                    await jobQuery.update(req);
                });
                resolve({status: "success", message: strings.jobAssignedSuccess})
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary Cancel a job
	 * @param { ExpressRequest} req - the default requets object
     * @param { jobId } req.body.jobId - the jobId to cancel
	 * @returns { Response } strings.jobCancelSuccess in res.data
	 */
    public async cancel(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                req.data = {jobId: req.body.jobId}
                const existing = await jobQuery.getById(req);
                if(existing[0].status >= cancelStatus) {
                    resolve({status: "error", message: strings.jobCancelError});
                }
                let updated: any = {};
                updated.isCancelled = true;
                updated.jobEndedDate = new Date(),
                updated.cancelledReason = req.body.cancelledReason || "";
                updated.cancelledNotes = req.body.cancelledNotes || "";
                updated.cancelledBy = req.authenticate.userId;
                req.data = {jobId: req.body.jobId, update: updated}
                await jobQuery.update(req);

                const assignemnts = await assignmentQuery.getByJob(req);
                if (assignemnts.length > 0) {
                    await assignmentQuery.complete(req);
                }
                resolve({status: "success", message: strings.jobCancelSuccess});
        	} catch(error) {
                console.log(error);
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary DeletE a job
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId to delete
	 * @returns { Response } strings.jobDeleteSuccess
	 */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                req.data = {jobId: req.body.jobId}
                const existing = await jobQuery.getById(req);
                if(existing[0].status > deleteStatus) {
                    resolve({status: "error", message: strings.jobDeleteError});
                } else {
                    await jobQuery.delete(req);
                    resolve({status: "success", message: strings.jobDeleteSuccess});
                }
        	} catch(error) {
        		reject(error);
        	};
        });
    }


   /**
	 * @public
	 * @async
	 * @summary Worker arrived on site
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId of which the user has arrived on site
     * @param { string } req.body.latitude - the latitude geo-position of the user
     * @param { string } req.body.longitude - the longitude geo-position of the user
	 * @returns { Response } strings.locationCalculationSuccess
	 */
    public async arriveOnSite(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId", "latitude", "longitude"]);
                req.data = {jobId: req.body.jobId}
                const existing = await jobQuery.getById(req);
                const distance = location(req.body.latitude, req.body.longitude, existing[0].addressLatitude, existing[0].addressLongitude);
                if(distance > distancePreferance) {
                    resolve({status: "error", message: strings.locationCalculationError});
                }
                req.data = {
                    jobId: req.body.jobId,
                    update: {
                        status: onSiteStatus
                    }
                }
                await jobQuery.update(req);
                let updatedAssignment: any = {};
                updatedAssignment.arrivedOnSiteDate = new Date();
                updatedAssignment.locationArrivedOnSite = `${req.body.latitude}:${req.body.longitude}`;
                req.data = {
                    jobId: req.body.jobId,
                    update: updatedAssignment
                }
                await assignmentQuery.update(req);
                resolve({status: "success", message: strings.locationCalculationSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

   /**
	 * @public
	 * @async
	 * @summary Worker has completed job on-site
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId of which the worker has completed
     * @param { string } req.body.latitude - the latitude geo-location of the user
     * @param { string } req.body.longitude - the longitude geo-location of the user
	 * @returns { Response } strings.
	 */
    public async onSiteComplete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId", "latitude", "longitude"]);
                req.data = {
                    jobId: req.body.jobId,
                    update: {
                        status: onSiteCompleted
                    }
                }
                const update = await jobQuery.update(req);
                let closeAssignment: any = {};
                closeAssignment.isCompleted = true;
                closeAssignment.completedOnSiteDate = new Date();
                closeAssignment.locationCompletedOnSite = `${req.body.latitude}:${req.body.longitude}`;
                req.data = {
                    jobId: req.body.jobId,
                    update: closeAssignment
                }
                const closeAssignmentQuery = await assignmentQuery.update(req);
                resolve({status: "success", message: ""});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary Office review and aproval
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId to approve
	 * @returns { Response } strings.jobReviewApproved
	 */
    public async officeReviewdApproved(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                let updateJob: any = {};
                updateJob.status = approvedStatus;
                updateJob.jobEndedDate = new Date();
                updateJob.completionApprovedBy = req.authenticate.userId;
                req.data = {jobId: req.body.jobId, update: updateJob}
                await jobQuery.update(req);

                req.body["actionName"] = "approved the job";
                req.body["actionType"] = "Approved"
                req.body["jobId"] = req.body.jobId;
                req.body["isSuccess"] = true;
                req.body["notes"] = req.body.notes || "";
                await jobActionClass.create(req);

                resolve({status: "success", message: strings.jobReviewApproved});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

   /**
	 * @public
	 * @async
	 * @summary Mark a job as invoiced
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId to invoice
	 * @returns { Response } 
	 */
    public async invoice(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                let updatedJob: any = {};
                updatedJob["status"] = invoicedStatus;
                updatedJob["isInvoiced"] = true;
                updatedJob["isCompleted"] = true;

                req.data = {
                    jobId: req.body.jobId,
                    update: updatedJob
                }
                await jobQuery.update(req);

                req.body["actionName"] = "invoiced the job";
                req.body["actionType"] = "Invoiced"
                req.body["jobId"] = req.body.jobId;
                req.body["isSuccess"] = true;
                req.body["notes"] = req.body.notes || "";
                await jobActionClass.create(req);

                resolve({status: "success", message: strings.jobInvoicedANdClosed});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Genrate a job report
    * @param { ExpressRequest } req - the default request object
    * @param { boolean } [req.body.email] - should the report be emailed to the customer
    * @return if req.body.email is true, it returns success in res.data. if req.body.email is false it
    * returns the file
    */
    public async genrateReport(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                req.data = {jobId: req.body.jobId}

                const details = await this.get(req);

                const job: Job = details.data.job[0];
                const assignments: Assignment = details.data.assignments;
                let contactPerson: any = {
                    firstName: "", lastName: ""
                };
                
                req.data = {priorityId: job.priorityId}
                const priorityFromQuery = await jobPriorityQuery.getSingle(req);
                const priority = priorityFromQuery[0];

                if (job.customerContactPersonId) {
                    req.data = {contactId: job.customerContactPersonId}
                    const contactPersonDetails = await contactPeopleQuery.getSingle(req);
                    contactPerson = contactPersonDetails[0];
                }
                
                
                let html = fs.readFileSync("./templates/job.report.html", "utf8");
                const options = { width: "785px", height: "1100px" };
                
                // replace data placeholders
                html=html.replace("#JOBID", ` ${job.jobId}`);
                html=html.replace("#JOBADDR", ` ${job.addressLineOne} </br> ${job.addressLineTwo} </br> ${job.addressPostcode}`);
                html=html.replace("#TENANTDETAIL", ` ${job.tenantFirstName} ${job.tenantLastName}`);
                html=html.replace("#REPORTDESC", ` ${job.jobDescription}`);
                html=html.replace("#CLIENTDETAILS", ` ${job.currentCustomerName}`);
                html=html.replace("#PROPERTYACCESS", ` ${job.accessDetails}`);
                html=html.replace("#NUMBERVISITS", ` `);
                html=html.replace("#NUMBERCALLS", ` `);
                html=html.replace("#JOBRAISED", ` ${contactPerson.firstName} ${contactPerson.lastName}`);
                html=html.replace("#PRIORITY", ` ${priority.priorityName}`);
                html=html.replace("#JOBCAT", ` ${job.category}`);
                html=html.replace("#ENGIEERID", ` ${assignments.userId}`);
                html=html.replace("#ENGINEERREPORT", ` ${job.jobNotes}`);
                html=html.replace("#WORKREQDATE", ` ${jsDateFormat(job.jobCreationDate)}`);
                html=html.replace("#WORKCOMDATE", ` ${jsDateFormat(job.jobEndedDate)}`);
                html=html.replace("#JOBSTARTDATE", `${jsDateFormat(assignments.arrivedOnSiteDate)}`);
                html=html.replace("#JOBFINISHDATE", ` ${jsDateFormat(assignments.completedOnSiteDate)}`);
                
                const pdf = await pdfClass.generate(html, options, `./files/reports/${job.jobId}.pdf`);

                if (req.body.email) {
                    // EMAIL FUNCTINALLITY HAS BEEN REMOVED FOR PRIVACY REASONS
                    resolve({status: "success", message: strings.jobReportSent});
                }
                
                resolve({status: "success", data: pdf});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default JobClass;