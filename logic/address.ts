import { ExpressRequest } from "../models/express";
import { Response } from "../models/response"; 
import { Address } from "../models/address";
import { AddressQuery } from "../queries/address";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import bulkRequestCheck from "../middleware/bulkRequestCheck";

const addressQuery = new AddressQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc AddressClass manages all logic relating to addresses
 */
export class AddressClass {

    /**
     * @async
     * @public
     * @summary Create an address
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.addressLineOne - first line of the address
     * @param { string } req.body.addressPostcoe - the address postcode
     * @param { string } req.body.addressCity - the address city
     * @param { string } req.body.addressBorugh - the address borugh
     * @param { string } req.body.customerId - the customerId to which the address belongs
     * @param { string } [req.body.addressLineTwo] - second line of the address
     * @param { string } [req.body.addressLineThree] - third line of the address
     * @param { string } [req.body.parkingNotes] - notes about parking restrictions at the address
     * @param { string } [req.body.parkingZone] - the parking zone the address is located in
     * @param { boolean } [req.body.isEstateParking] - is there estate parking availble for contractors
     * @returns { string } strings.addressCreatedSuccess
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["addressLineOne", "addressPostcode", "addressCity", "addressBorugh", "udprn", "latitude", "longitude"]);
                const address: Address = {
                    addressLineOne: req.body.addressLineOne,
                    addressLineTwo: req.body.addressLineTwo || "",
                    addressLineThree: req.body.addressLineThree || "",
                    addressPostcode: req.body.addressPostcode,
                    addressCity: req.body.addressCity,
                    addressBorugh: req.body.addressBorugh,
                    parkingNotes: req.body.parkingNotes || "",
                    parkingZone: req.body.parkingZone || "",
                    isEstateParking: req.body.isEstateParking || false,
                    isActive: true,
                    addressId: uuid(),
                    ownerId: req.authenticate.userId,
                    udprn: req.body.udprn,
                    longitude: req.body.longitude,
                    latitude: req.body.latitude
                }
                req.data = address;
                const createQuery = await addressQuery.create(req);
                resolve({status: "success", message: strings.addressCreatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @async
     * @public
     * @summary Get a single address by Id
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.addressId - the addressId to get
     * @returns { Response } an array with one address in the data property
     */
    public async getAddressByUdprn(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["udprn"]);
                req.data = {udprn: req.body.udprn}
                const address = await addressQuery.getSingleByUdprn(req);
                if (address.length < 1) {
                    
                }
                resolve({status: "success", data: {address}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get addresses with custom filters, sorting, and pagnation
     * @param { ExpressRequest } req - the default request object
     * @param { number } [req.body.opts.limit] - limit the number of results
     * @param { number } [req.body.opts.offset] - offset position
     * @param { string } [req.body.opts.orderBy] - how to order results ASC or DESC
     * @param { orderByField } [req.body.opts.orderByField] - the property to order by
     * @param { boolean } [req.body.opts.activated] - should the query return deactivated results
     * @returns { Response } an array of addresses and an array with the count of results in the data object 
     */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
        		let limit: number = 0;
                let offset: number = 10;
                let orderBy: string = "ASC";
                let orderByField: string = "addressLineOne";
                let queryString: string;
                let countQueryString: string;
                let activated = true;

                if(req.body.opts) {
                    if(req.body.opts.offset != undefined) {offset = req.body.opts.offset};
                    if(req.body.opts.limit != undefined) {limit = req.body.opts.limit};
                    if(req.body.opts.orderBy != undefined) {orderBy = req.body.opts.orderBy};
                    if(req.body.opts.orderByField != undefined) {orderByField = req.body.opts.orderByField};  
                    if(req.body.opts.deactivated != undefined) {activated = req.body.opts.deactivated};
                }

                queryString = `select addresses.addressLineOne, addresses.addressLineTwo, addresses.addressLineThree, addresses.addressPostcode, addresses.addressCity, addresses.addressBorugh, addresses.parkingNotes, addresses.parkingZone, addresses.isEstateParking, addresses.isActive, addresses.addressId, addresses.customerId, addresses.ownerId, customers.companyName from addresses join customers on customers.customerId = addresses.customerId where addresses.isActive = ${activated} order by addresses.${orderByField} ${orderBy} limit ${limit}, ${offset};`;
                countQueryString = `select count(addressLineOne) as 'count' from addresses where isActive = ${activated};`;

                if(req.body.opts) {
                    if(req.body.opts.field != undefined) {
                        queryString = `select addresses.addressLineOne, addresses.addressLineTwo, addresses.addressLineThree, addresses.addressPostcode, addresses.addressCity, addresses.addressBorugh, addresses.parkingNotes, addresses.parkingZone, addresses.isEstateParking, addresses.isActive, addresses.addressId, addresses.customerId, addresses.ownerId, customers.companyName from addresses join customers on customers.customerId = addresses.customerId where addresses.isActive = ${activated} and addresses.${req.body.opts.field} like "%${req.body.opts.searchterm}%" order by addresses.${orderByField} ${orderBy} limit ${limit}, ${offset};`;
                        countQueryString = `select count(addressLineOne) as 'count' from addresses where isActive = ${activated} and ${req.body.opts.field} like "%${req.body.opts.searchterm}%";`
                    }
                }

                const addresses = await addressQuery.getCustom(queryString);
                const count = await addressQuery.getCustom(countQueryString);

                resolve({status: "success", data: {addresses, count}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @param
     * @async
     * @summary Update an address
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.addressId - the addressId to update
     * @returns { Response } if there are no updated properties it returns strings.noUpdatedProperties
     * @returns { Response } if the update is successfull it returns strings.addressUpdatedSuccess
     */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["udprn"]);
                req.data = {udprn: req.body.udprn};
                const existing = await addressQuery.getSingleByUdprn(req);
                let updated: any = {};
                for(let [key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        updated[key] = req.body[key];
                    }
                }
                if(Object.keys(updated).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }
                req.data = {
                    update: updated,
                    udprn: req.body.udprn
                }
                const update = await addressQuery.update(req);
                resolve({status: "success", message: strings.addressUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Activate an address
     * @param { ExpressRequest } req - the default request object
     * @param { stirng } req.body.addressId - the addressId to activate
     * @returns { Response } strings.addressActivatedSuccess
     */
    public async activate(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["addressId"]);
                req.data = {addressId: req.body.addressId}
                const activate = await addressQuery.activate(req);
                resolve({status: "success", message: strings.addressActivatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary - Deactivate an address
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.addressId - the addresId to deactivate
     *  @returns { Response } strings.addressDeactivatedSuccess
     */
    public async deactivate(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["addressId"]);
                req.data = {addressId: req.body.addressId}
                const deactivate = await addressQuery.deactivate(req);
                resolve({status: "success", message: strings.addressDeactivatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Delete an address
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.addressId - the addressId to delete
     * @returns { Response } strings.addressDeletedSuccess
     */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["addressId"]);
                req.data = {addressId: req.body.addressId}
                const deleteQuery = await addressQuery.delete(req);
                resolve({status: "success", message: strings.addressDeletedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default AddressClass