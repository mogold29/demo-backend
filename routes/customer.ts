import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { Customer } from "../models/customer";
import { CustomerClass } from "../logic/customer";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

const customerClass = new CustomerClass();

routes.post("/customers/create", async(req: ExpressRequest, res: any) => {
    try {
        await Permission(req, "create", "customers");
        const create = await customerClass.create(req);
        Responses.success(res, create);
    } catch(error) {
        console.log(error);
        Responses.error(res, error);
    }
});

routes.post("/customers/createBulk", async(req: ExpressRequest, res: any) => {
    try {
        await Permission(req, "create", "customers");
        const createBulk = await customerClass.createBulk(req);
        Responses.success(res, createBulk);
    } catch(error) {
        Responses.error(res, error);
    }
});

routes.post("/customers/getSingleById", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "customers", req.body.customerId);
    	const get = await customerClass.getSingleById(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/getSingleByName", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "customers");
    	const get = await customerClass.getSingleByName(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "customers");
    	const get = await customerClass.get(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "customers", req.body.customerId);
    	const update = await customerClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/deactivate", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "deactivate", "customers", req.body.customerId);
    	const deactivate = await customerClass.deactivate(req);
    	Responses.success(res, deactivate);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/activate", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "activate", "customers", req.body.customerId);
    	const activate = await customerClass.activate(req);
    	Responses.success(res, activate);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/addfile", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "addFile", "customers", req.body.customerId);
    	const addFile = await customerClass.addFile(req);
    	Responses.success(res, addFile);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/getfile", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "getFile", "customers", req.body.customerId);
    	const getFile = await customerClass.getFiles(req);
    	Responses.success(res, getFile);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/deletFile", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "deleteFile", "customers", req.body.customerId);
    	const deleteFile = await customerClass.deleteFile(req);
    	Responses.success(res, deleteFile);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/customers/getreps", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "getRep", "customers", req.body.customerId);
    	const getReps = await customerClass.getReps(req);
    	Responses.success(res, getReps);
    } catch(error) {
    	Responses.error(res, error);
    }
});
routes.post("/customers/updatenotes", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "basicUpdate", "customers", req.body.customerId);
    	const update = await customerClass.updateNotes(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;