import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import { User } from "../models/auth/user";

import { UserClass } from "../logic/auth/user";
const userClass = new UserClass();

const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);
const randomNumber = Math.floor(100000000 + Math.random() * 900000000);

describe("User Tests", () => {
    let req: any = {
    	headers: {
    		sessionToken: "a328ff82-e176-4220-84fe-9505c2ba1ff3"
    	}
    };
    let res: any = {};

    describe("Create users", () => {
        it("should create a user successfully", async() => {
            
            const user: User = {
                firstName: "Test",
                lastName: "Test",
                emailAddress: `${random}@gmail.com`,
                phoneNumber: randomNumber.toString(),
                password: random,
                tfaEnabled: false,
                forceChangePassword: false,
                userId: uuid(),
                ownerId: uuid(),
                userManager: uuid(),
                roleId: uuid(),
                shortId: randomNumber
            }

            req.body = user;
            
            const result = await userClass.create(req);
            assert.equal(result.status, "success");
        });

        it("should error with duplicate email address", async() => {
            const user: User = {
                firstName: "Test",
                lastName: "Test",
                emailAddress: `${random}12@gmail.com`,
                phoneNumber: `${randomNumber.toString()}1`,
                password: random,
                tfaEnabled: false,
                forceChangePassword: false,
                userId: uuid(),
                ownerId: uuid(),
                userManager: uuid(),
                roleId: uuid(),
                shortId: randomNumber
            }

            req.body = user;
            
            await userClass.create(req).catch(error => {
                expect(() => {throw error}).to.throw(Error, "")
            });
        });
    });

})