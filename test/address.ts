import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { Address } from "../models/address";
import { AddressClass } from "../logic/address";

import { Customer } from "../models/customer";
import { CustomerClass } from "../logic/customer";

const addressClass = new AddressClass();
const customerClass = new CustomerClass();

describe("Address tests", async() => {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);
    const randomNumber = Math.floor(100000000 + Math.random() * 900000000);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        }
    };
    let res: any = {};
   

    describe("Create addresses", async() => {
        it("should create an address successfully with all properties", async() => {
            const address: Address = {
                addressLineOne: random,
                addressLineTwo: random,
                addressLineThree: random,
                addressCity: random,
                addressPostcode: random,
                addressBorugh: random,
                addressId: uuid(),
                ownerId: req.authenticate.userId,
                isActive: true, 
                isEstateParking: false,
                parkingNotes: random,
                udprn: randomNumber,
                latitude: random,
                longitude: random
            }
            req.body = address;
            const create = await addressClass.create(req);
            assert.equal(create.message, strings.addressCreatedSuccess);
        });
        it("should create an address with all REQUIRED properties", async() => {
            const address: Address = {
                addressLineOne: random,
                addressCity: random,
                addressId: uuid(),
                addressPostcode: random,
                ownerId: req.authenticate.userId,
                addressBorugh: random,
                udprn: randomNumber,
                latitude: random,
                longitude: random
            }
            req.body = address;
            const create = await addressClass.create(req);
            assert.equal(create.message, strings.addressCreatedSuccess);
        });
        it("should error when missing required properties", async() => {
            const address = {
                addressCity: random,
                addressId: uuid(),
                addressPostcode: random,
                ownerId: req.authenticate.userId
            }
            req.body = address;
            addressClass.create(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "property addressLineOne is missing and is required, please check and try again")
            });
        });
    });

    describe("Get addresses", async() => {
        it("should get an address by Id successfuly", async() => {
            const existingAddresses = await addressClass.get(req);
            const addressId = existingAddresses.data.addresses[0].addressId;
            req.body = {addressId}
            const get = await addressClass.getAddressByUdprn(req);
            assert.isArray(get.data.address)
            assert.equal(get.status, "success");
        });
        it("should get addresses with custom query", async() => {
            const get = await addressClass.get(req);
            assert.equal(get.status, "success");
        });
    });

    describe("Update an address", async() => {
        it("should upate an address successfully", async() => {
            const existingAddress = await addressClass.get(req);
            const existinName = existingAddress.data.addresses[0].companyName;
            const existingId = existingAddress.data.addresses[0].addressId;
            req.body = {addressId: existingId, addressLineOne: random}
            const update = await addressClass.update(req);
            assert.equal(update.status, "success");
            assert.equal(update.message, strings.addressUpdatedSuccess);
        });
    })
})