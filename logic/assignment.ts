import { Response } from "../models/response";
import { AssignmentQuery } from "../queries/assignemnt";
import { JobClass } from "./job";
import distance from "../middleware/location";
import { JobActionClass } from "./jobAction";;

const assignmentQuery = new AssignmentQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc AssignmentClass is the logic for assignments
 */
export class AssignmentClass {

    /**
     * MOST OF THE FILE HAS BEEN DELETED TO PRESERVE SECURITY AND PRIVACY
     * THE BELOW FUNCTION WAS A COMPLICATED FUNCTION AT THE TIME OF WRITING
     * AND TOOK SOME FIGURING OUT TO GET IT RIGHT
     */


    /**
    * @public
    * @async
    * @summary reccomend jobs to be assigneD
    * @return an array of jobs
    */
    public async recommend() {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const assignments = await assignmentQuery.recommend();
                // set an outer array to hold all grouped arrays
                // arrays are grouped by 5 mile radius - this will
                // be changed down the line to make dynamic
                const outerArray = [];
                while (assignments.length > 0) {
                    const first = assignments[0];
                    // temorary array for the current group this gets pushed
                    // into the outerArray when the for loop completes
                    let tempArray = [];
                    // reset i to 0 when the for loop completes, otherwise
                    // the index will crash after the first group completes
                    // because it will be looking for ex: index 15 which has
                    // been spliced during the first round, so we reset
                    let i = 0;
                    // on start set to i to 0; and loop until the assignents array
                    // is empty, we do not increment i on every loop
                    for (let i = 0; i < assignments.length;) {
                        const xDistance = distance(first.addressLatitude, first.addressLongitude, assignments[i].addressLatitude, assignments[i].addressLongitude) / 5280;
                        if (Math.round(xDistance) <= 5) {
                            tempArray.push(assignments[i]);
                            assignments.splice(i, 1);
                        } else {
                            i++;
                        }
                    }
                    // once its reached the end of the array, push the tempArray into the outer array
                    // and reset the temp array at this point i gets reset to 0 and the for loop restarts
                    outerArray.push(tempArray);
                    tempArray = [];
                }
                // finaly resolve with the outer array.
                resolve({status: "success", data: {assignments: outerArray}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default AssignmentClass;