const env = require("./env/env");

import express from "express";
const app = express();

import routes from "./routes";
app.use(routes.routes);

import bodyParser from "body-parser";
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const cookieParser = require("cookie-parser");
app.use(cookieParser());

const fileUpload = require("express-fileupload");
app.use(fileUpload);

const http = require("http").Server(app);
http.listen(env.env.port);
console.log(`I can be found at http://localhost:${env.env.port}`);