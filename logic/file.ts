import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import { File } from "../models/file";
import { FileQuery } from "../queries/file";
const IncomingForm = require("formidable").IncomingForm;
const fs = require("fs");

const fileQuery = new FileQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc FileClass manages all files
 */
export class FileClass {

    /**
     * @public
     * @async
     * @summary Create a new file
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.fileType - the file type example "customer" or "job"
     * @param { stirng } req.body.fileOwner - the entityId owning the file such as "customerId" or "jobId"
     * @returns { Response } strings.fileUploadSuccess
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                if (!req.params.id || !req.params.type) {
                    resolve({status: "error", message: strings.missingParamsError});
                }
                req.body["fileType"] = req.params.type;
                req.body["fileOwner"] = req.params.id;
                await requestCheck(req, ["fileType", "fileOwner"]);

                const form = new IncomingForm();

                form.on("file", async(field: any, file: any) => {
                    const id = uuid();
                    fs.copyFileSync(file.path, `./files/${id}.${file.name.split(".")[1]}`);
                    const currentFile: any = {};
                    currentFile["id"] = id;
                    currentFile["extension"] = file.name.split(".")[1];

                    const newFileObject: File = {
                        fileId: currentFile.id,
                        fileType: req.body.fileType,
                        fileName: file.name,
                        fileExtension: currentFile.extension,
                        fileOwner: req.body.fileOwner,
                        fileTags: req.body.fileTagw || ""
                    }

                    req.data = newFileObject
                    await fileQuery.create(req);
                });
                form.parse(req);
                resolve({status: "success", message: strings.fileUploadSuccess});
        	} catch(error) {
                console.log(error)
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get files
     * @param { ExpressRequest } req - the default request object
     * @param { string } fileType - the fileType to get ex: "customer"
     * @param { string } fileOwner - the entityId that owns the file
     * @returns { Response } an array of file paths
     */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["fileType", "fileOwner"]);
                req.data = {
                    fileType: req.body.fileType, 
                    fileOwner: req.body.fileOwner
                }
                const files = await fileQuery.get(req);
                resolve({status: "success", data: {files}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Download a file
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.fileId - a file Id
    * @return a file
    */
    public async download(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["fileId"]);
                req.data = {fileId: req.body.fileId}
                const file = await fileQuery.getSingle(req);
                const filepath = `./files/${file[0].fileId}.${file[0].fileExtension}`;
                resolve({status: "success", data: filepath});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Delete a file
     * @param { ExpressRequest } req - the default request object
     * @param { string }  req.body.fileId - the fileId to delete
     * @param { string } req.body.fileOwner - the entityId that owns the file
     * @returns { Response } strings.fileDeleteSuccess
     */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["fileId", "fileOwner"]);
                req.data = {
                    fileId: req.body.fileId,
                    fileOwner: req.body.fileOwner
                }
                const file = await fileQuery.getSingle(req);
                await fileQuery.delete(req);
                fs.unlinkSync(`./files/${file[0].fileId}.${file[0].fileExtension}`)
                resolve({status: "success", message: strings.fileDeleteSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default FileClass;
