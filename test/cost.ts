import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { Cost } from "../models/cost";
import { CostClass } from "../logic/cost";
const costClass = new CostClass();

describe("Cost tests", async() => {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        body: {}
    };
    let res: any = {};
    const jobId = "66a4a231-42fd-430a-b68b-89c63de22948";

    describe("Create costs", async() => {
        it("should create a cost", async() => {
            const cost: Cost = {
                category: "abc",
                cost: 12.50,
                costId: uuid(),
                userId: req.authenticate.userId,
                ownerId: req.authenticate.userId,
                jobId,
                dateSubmitted: new Date(),
                description: uuid()
            }
            req.body = cost;
            const create = await costClass.create(req);
            assert.equal(create.status, "success");
        });
    });

    describe("create bulk costs", async() => {
        it("should create bulk costs", async() => {
            const costs = [
                {
                    description: uuid(),
                    category: "abc",
                    cost: 12.50,
                    jobId,
                },
                {
                    description: uuid(),
                    category: "abc",
                    cost: 12.50,
                    jobId,
                }
            ];
            req.body = {costs}
            const create = await costClass.createBulk(req);
            assert.equal(create.status, "success");
        });
    });

    describe("get costs", async() => {
        it("should get all costs for a job", async() => {
            req.body = {jobId}
            const get = await costClass.getByJob(req);
            assert.isArray(get.data.costs);
        });
        it("should get costs of user by job", async() => {
            req.body = {
                jobId,
                userId: req.authenticate.userId
            }
            const get = await costClass.getByJobAndUser(req);
            assert.isArray(get.data.costs);
        })
    });

    describe("Approve or reject costs", async() => {
        it("should approve all costs of a job", async() => {
            req.body = {jobId}
            const approve = await costClass.approveByJob(req);
            assert.equal(approve.status, "success");
        });
        it("shold reject all cost of a job", async() => {
            req.body = {jobId}
            const approve = await costClass.rejectByJob(req);
            assert.equal(approve.status, "success");
        });
        it("should apprve by job and user", async() => {
            req.body = {jobId, userId: req.authenticate.userId}
            const approve = await costClass.approveByJobAndUser(req);
            assert.equal(approve.status, "success");
        });
        it("should reject all jobs of a job and user", async() => {
            req.body = {jobId, userId: req.authenticate.userId}
            const approve = await costClass.rejectByJobAndUser(req);
            assert.equal(approve.status, "success");
        });
    });

    describe("update costs", async() => {
        it("should update a cost", async() => {
            req.body = {jobId}
            const existing = await costClass.getByJob(req);
            const firstCost = existing.data.costs[0];
            const cost = {
                costId: firstCost.costId,
                description: uuid()
            }

            req.body = cost;
            const update = await costClass.update(req);
            assert.equal(update.status, "success");
        });
    });

    describe("delete costs", async() => {
        it("should delete a cost", async() => {
            req.body = {jobId}
            const existing = await costClass.getByJob(req);
            const firstCost = existing.data.costs[0];
            req.body = {costId: firstCost.costId,}
            const deleteCost = await costClass.delete(req);
            assert.equal(deleteCost.status, "success");
        });
    });
})