import { SessionQuery as sq } from "../queries/auth/session";
import { ExpressRequest } from "../models/express";

const SessionQuery = new sq();

export default function(req: ExpressRequest, res: any, next: any) {
    return new Promise(async(resolve, reject) => {
        try {
            if(req.headers.authorization == undefined) {
                next();
            } else {
                req.authenticate["sessionToken"] = req.headers.authorization;
                const sessionQuery = await SessionQuery.get(req);
                req.authenticate["userId"] = sessionQuery[0].userId;
                next();
            }
        } catch(error) {
            reject(error);
        }
    })
}