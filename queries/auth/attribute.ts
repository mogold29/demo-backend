import query from "../../middleware/query";
import { Attribute } from "../../models/auth/attribute";
import { ExpressRequest } from "../../models/express";

export class AttributeQuery {

    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const createQuery = await query("insert into attributes set ?", req.data);
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const updateQuery = await query("update attributes set ? where attribute = '" + req.data.attribute + "';", req.data.update);
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    public async delete(req:ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const deleteQuery = await query("delete from attributes where attribute = '" + req.data.attribute + "';", null);
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    public async get() {
        return new Promise<Array<Attribute>>(async(resolve, reject) => {
            try {
                const getQuery = await query("select * from attributes", null);
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }
}