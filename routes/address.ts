import { ExpressRequest } from "../models/express";
import { AddressClass } from "../logic/address";
import { IdealPostcodeClass } from "../logic/idealPostcodes";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

const addressClass = new AddressClass();
const idealPostcodeClass = new IdealPostcodeClass();

routes.post("/address/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "addresses");
    	const create = await addressClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/address/getSingleByUdprn", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "addresses", req.body.addressId);
    	const getAddress = await addressClass.getAddressByUdprn(req);
    	Responses.success(res, getAddress);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/address/search", async(req: ExpressRequest, res: any) => {
	try {
		const search = await idealPostcodeClass.search(req);
		Responses.success(res, search);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/address/select", async(req: ExpressRequest, res: any) => {
	try {
		const select = await idealPostcodeClass.getAddressByUdprn(req);
		Responses.success(res, select);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/address/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "addresses");
    	const getAddress = await addressClass.get(req);
    	Responses.success(res, getAddress);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/address/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "addresses", req.body.addressId);
    	const update = await addressClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/address/activate", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "activate", "addresses", req.body.addressId);
    	const activate = await addressClass.activate(req);
    	Responses.success(res, activate);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/address/deactivate", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "deactivate", "addresses", req.body.addressId);
    	const deactivate = await addressClass.deactivate(req);
    	Responses.success(res, deactivate);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/address/delete", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "delete", "addresses", req.body.addressId);
    	const deleteAddress = await addressClass.delete(req);
    	Responses.success(res, deleteAddress);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;