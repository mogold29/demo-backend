import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { Cost } from "../models/cost";
import { CostClass } from "../logic/cost";

const costClass = new CostClass();

routes.post("/cost/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "costs");
    	const create = await costClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/createbulk", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "costs");
    	const create = await costClass.createBulk(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "costs", req.body.costId);
    	const update = await costClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/getbyjob", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "costs");
		const get = await costClass.getByJob(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/cost/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "costs", req.body.costId);
    	const get = await costClass.getByJobAndUser(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/delete", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "delete", "costs", req.body.costId);
    	const deleteFunction = await costClass.delete(req);
    	Responses.success(res, deleteFunction);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/approve", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "approve", "costs", req.body.costId);
    	const approve = await costClass.approve(req);
    	Responses.success(res, approve);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/reject", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "approve", "costs", req.body.costId);
    	const reject = await costClass.reject(req);
    	Responses.success(res, reject);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/approvejob", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "approve", "costs");
    	const approve = await costClass.approveByJob(req);
    	Responses.success(res, approve);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/rejectjob", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "approve", "costs");
    	const reject = await costClass.rejectByJob(req);
    	Responses.success(res, reject);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/approvejobuser", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "approve", "costs");
    	const approve = await costClass.approveByJobAndUser(req);
    	Responses.success(res, approve);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/cost/rejectjobuser", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "approve", "costs");
    	const reject = await costClass.rejectByJobAndUser(req);
    	Responses.success(res, reject);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;