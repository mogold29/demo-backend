import { ExpressRequest } from "../../models/express";
import { Response } from "../../models/response";
import { SessionClass as sc } from "./session";
import bcrypt from "bcrypt";

const sessionClass = new sc();

export class LoginClass {

    public async login(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                const createSession = await sessionClass.create(req);
                resolve({status: "success", data: {sessionToken: createSession.data.token}});
            } catch(error) {
                reject(error);
            }
        })
    }

}