export interface Company {
    companyName: string,
    addressLineOne: string,
    addressLineTwo?: string,
    addressLineThree?: string,
    addressPostcode: string,
    city: string,
    udprn: number,
    companyNumber: number,
    officeEmail: string,
    officePhone: string,
    setUp: boolean,
    setupToken?: string
}