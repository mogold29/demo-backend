import { Customer } from "../models/customer";
import { CustomerQuery } from "../queries/customer";
import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import requestCheck from "../middleware/requestCheck";
import bulkRequestCheck from "../middleware/bulkRequestCheck";
import { uuid } from "uuidv4";
import strings from "../strings";
import { ContactPepoleQuery } from "../queries/contactPepole";
import { JobQuery } from "../queries/job";
import { FileClass } from "./file";
import route from "../routes/customer";

const fileClass = new FileClass();
const jobQuery = new JobQuery();
const customerQuery = new CustomerQuery();
const contactPepoleQuery = new ContactPepoleQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc CustomerClass manages all logic for customers
 */
export class CustomerClass {

    /**
     * @public
     * @async
     * @summary Create a new customer
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.companyName - the legal company name mut be unique
     * @param { string } req.body.addressLineOne - the first line of the address
     * @param { string } req.body.addressPostcode - the postcode of the address
     * @param { string } req.body.city - the address city
     * @param { string } req.body.officePhone - the main switchboard phone number
     * @param { string } req.body.officeEmail - the main email address
     * @param { number } req.body.companyNumber - the legal company number must be unique 
     * @param { string } [req.body.addressLineTwo] - the second line of the address
     * @param { string } [req.body.addressLineThree] - the third line of the address
     * @param { string } [req.body.notes] - any notes for the customer
     * @returns { Response } strings.customerSuccess
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                const check = await requestCheck(req, ["companyName", "addressLineOne", "addressPostcode", "addressCity", "officePhone", "officeEmail", "companyNumber"]);

                const customer: Customer = {
                    companyName: req.body.companyName,
                    addressLineOne: req.body.addressLineOne,
                    addressLineTwo: req.body.addressLineTwo || "",
                    addressLineThree: req.body.addressLineThree || "",
                    addressPostcode: req.body.addressPostcode,
                    addressBorugh: req.body.addressBorugh || "",
                    udprn: req.body.udprn,
                    longitude: req.body.longitude,
                    latitude: req.body.latitude,
                    addressCity: req.body.addressCity,
                    officePhone: req.body.officePhone || "",
                    officeEmail: req.body.officeEmail || "",
                    companyNumber: req.body.companyNumber,
                    active: true,
                    notes: req.body.notes || "",
                    customerId: uuid(),
                    ownerId: req.authenticate.userId
                }

                req.data = customer;
                const createQuery = await customerQuery.create(req);
                resolve({status: "success", message: strings.customerSuccess});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Craete customer in bulk
     * @param { ExpressRequest } req - the default request object
     * @param { Array<Customer> } req.body.customers - an array of customers
     * @param { string } req.body.customers.companyName - the company name must be unique
     * @param { string } req.body.customers.addressLineOne - the first line of the address
     * @param { string } req.body.customers.addressPostcode - the address postcode
     * @param { string } req.body.customers.addressCity - the address city
     * @param { string } req.body.customers.officePhone - the main switchboard office phone number
     * @param { string } req.body.customers.officeEmail - the main email address
     * @param { number } req.body.customers.companyNumber - the leagl company number must be unique
     * @returns { Response } strings.bulkCustomerSuccess
     */
    public async createBulk(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                let bulkArray: any = [[]];
                const check = await bulkRequestCheck(req, ["companyName", "addressLineOne", "addressPostcode", "addressCity", "officePhone", "officeEmail", "companyNumber"], "customers");
                req.body.customers.forEach((customer: Customer) => {
                    const xCustomer = [
                        customer.companyName,
                        customer.addressLineOne,
                        customer.addressPostcode,
                        customer.addressPostcode,
                        customer.officePhone || "",
                        customer.officeEmail || "",
                        customer.companyNumber,
                        true,
                        uuid(),
                        req.authenticate.userId
                    ];
                    bulkArray[0].push(xCustomer);
                });
                req.data = {customers: bulkArray}
                const createQuery = await customerQuery.createBulk(req);
                resolve({status: "success", message: strings.bulkCustomerSuccess});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get customers with custom filters, sorting, and pagnation
     * @param { ExpressRequest } req - the default request object
     * @param { number } [req.body.opts.limit] - limit the number of results
     * @param { number } [req.body.opts.offset] - offset position
     * @param { string } [req.body.opts.orderBy] - how to order results ASC or DESC
     * @param { orderByField } [req.body.opts.orderByField] - the property to order by
     * @param { boolean } [req.body.opts.activated] - should the query return deactivated results
     * @returns { Response } an array of customers and an array with the count of results in the res.data  
     */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                let offset: number = 10;
                let limit: number = 0;
                let orderBy: string = "ASC";
                let orderByField: string = "companyName";
                let queryString: string;
                let countQueryString: string;
                let activated = "where customers.active not like false and";

                if(req.body.opts != undefined) {
                    if(req.body.opts.offset != undefined) {offset = req.body.opts.offset};
                    if(req.body.opts.limit != undefined) {limit = req.body.opts.limit};
                    if(req.body.opts.orderBy != undefined) {orderBy = req.body.opts.orderBy};
                    if(req.body.opts.orderByField != undefined) {orderByField = req.body.opts.orderByField};  
                    if(req.body.opts.deactivated != undefined) {
                        console.log(req.body.opts.deactivated);
                        req.body.opts.deactivated ? activated = "where" : activated = activated;
                    };
                }
                queryString = `select customers.*, count(jobs.jobId) as 'activeJobs' from customers left join jobs on jobs.customerId = customers.customerId ${activated} group by customers.customerId order by customers.${orderByField} ${orderBy} limit ${offset} offset ${limit};`;
                countQueryString = `select count(companyName) as 'count' from customers ${activated};`;

                if(req.body.opts) {
                    if(req.body.opts.field != undefined) {
                        queryString = `select customers.*, count(jobs.jobId) as 'activeJobs' from customers left join jobs on jobs.customerId = customers.customerId ${activated} customers.${req.body.opts.field} like "%${req.body.opts.searchterm}%" group by customers.customerId order by customers.${orderByField} ${orderBy} limit ${offset} offset ${limit};`;
                        countQueryString = `select count(companyName) as 'count' from customers ${activated} ${req.body.opts.field} like "%${req.body.opts.searchterm}%";`
                    }
                }

                const customers = await customerQuery.get(queryString);
                const getCountQuery = await customerQuery.get(countQueryString);

                resolve({status: "success", data: {customers, getCountQuery}});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get a single customer
     * @param { ExpressRequest } req - the default request obejct
     * @param { string } req.body.customerId - the customerId to get
     * @returns { Response } an array of customers in the res.data 
     */
    public async getSingleById(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                const check = await requestCheck(req, ["customerId"]);
                req.data = {customerId: req.body.customerId}
                const customers = await customerQuery.getSingleById(req);
                resolve({status: "success", data: {customers}});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get cusotmer matching a name provided
     * @param { ExpressRequest } req - the default request object 
     * @param { string } req.body.companyName - the company name to search by
     * @returns { Response } an array of customers with reps in the res.data
     */
    public async getSingleByName(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                await requestCheck(req, ["companyName"]);
                req.data = {companyName: req.body.companyName}
                const customers = await customerQuery.getSingleByName(req);
                resolve({status: "success", data: {customers}});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get all reps of a company
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId to get the reps of
     * @returns { Response } an array of reps in the res.data 
     */
    public async getReps(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try{
                const check = await requestCheck(req, ["customerId"]);
                req.data = {customerId: req.body.customerId}
                const reps = await contactPepoleQuery.getByCustomer(req);
                resolve({status: "success", data: {reps}});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Update a customer
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId to update
     * @returns { Response } strings.customerUpdatedSuccess
     */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                await requestCheck(req, ["customerId"]);
                req.data = {customerId: req.body.customerId}
                const existingCustomer: any = await customerQuery.getSingleById(req);
                let updatedCustomer: any = {};
                for(let [key, val] of Object.entries(req.body)) {
                    if(existingCustomer[0][key] !== req.body[key]) {
                        updatedCustomer[key] = req.body[key]
                    }
                }
                if(Object.keys(updatedCustomer).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }

                req.data = {
                    update: updatedCustomer,
                    customerId: req.body.customerId
                }
                await customerQuery.update(req);
                resolve({status: "success", message: strings.customerUpdatedSuccess});
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Activate a customer
     * @param { ExpressRequest } req - the default request object
     * @returns { Response }  strings.customerActivatedSuccess
     */
    public async activate(req: ExpressRequest) {
        return new Promise<Response>(async(resolve, reject) => {
            try {
                const check = await requestCheck(req, ["customerId"]);
                req.data = {customerId: req.body.customerId}
                const activateQuery = await customerQuery.activate(req);
                resolve({status: "success", message: strings.customerActivatedSuccess});
            } catch(error) {
                reject(error)
            }
        })
    }

    /**
     * @public
     * @summary Deactivate a customer
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId to deactivate
     * @param { string } req.body.deactivationReason - a reason for the deactivation reason
     * @returns { Response } strings.customerDeactivatedSuccess
     */
    public async deactivate(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["customerId", "deactivationReason"]);
                req.data = {
                    customerId: req.body.customerId,
                    deactivationReason: req.body.deactivationReason,
                    deactivationNotes: req.body.deactivationNotes || ""
                }
                const deactivateQuery = await customerQuery.deactivate(req);
                resolve({status: "success", message: strings.customerDeactivatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Update customer notes
     * @param { ExpressRequest } req - the default request obejct
     * @param { string } req.body.customerId - the customerId to update
     * @param { string } req.body.notes - the updated notes
     * @returns { Response } the updated notes in the res.data 
     */
    public async updateNotes(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["customerId", "notes"]);
                req.data = {customerId: req.body.customerId}
                const existingNotes = await customerQuery.getSingleById(req);
                if(!existingNotes[0].notes === req.body.notes) {
                    req.body = {
                        customerId: req.body.customerId,
                        notes: req.body.notes
                    }
                    const updateQuery = await customerQuery.updateNotes(req);
                }
                resolve({status: "success", data: {notes: req.body.notes}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get customer files
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId to get files of
     * @returns { Response } an array of file links in the res.data 
     */
    public async getFiles(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["customerId"]);
                req.body["fileOwner"] = req.body.customerId;
                req.body["fileType"] = "customer";
                const files = await fileClass.get(req);
                resolve({status: "success", data: {files: files.data.files}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Add a new file to the customer
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId of which to add the file to
     * @returns { Response } strings.fileUploadSuccess 
     */
    public async addFile(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["customerId"]);
                req.body["fileType"] = "customer";
                req.body["fileOwner"] = req.body.customerId;
                const uploadFiles = await fileClass.create(req);
                resolve({status: "success", message: strings.fileUploadSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Delete a file
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the customerId of which to delete the file
     * @param { string } req.body.fileId - the fileId to delete
     * @returns { Response } strings.fileDeleteSuccess
     */
    public async deleteFile(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["customerId", "fileId"]);
                const deleteFile = await {}
                resolve({status: "success", message: strings.fileDeleteSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }


}

export default CustomerClass;