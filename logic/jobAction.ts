import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import bulkRequestCheck from "../middleware/bulkRequestCheck";
import { JobAction } from "../models/jobAction";
import { JobActionQuery } from "../queries/jobAction";

const jobActionQuery = new JobActionQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc JobActionClass managed job actions
 */
export class JobActionClass {

    /**
	 * @public
	 * @async
	 * @summary Create a job action
	 * @param { ExpressRequest} req - the efault requets object
     * @param { string } req.body.actionName - the name of the action such as "call" or "email"
     * @param { string } req.body.jobId - the jobId to which the action is linked to
     * @param { boolean } req.body.isSuccess - if the action completed successfully
     * @param { string } req.body.notes - notes realted to the action
	 * @returns { Response } strings.jobActionCreatedSuccess
	 */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["actionName", "jobId", "isSuccess", "notes", "actionType"]);
                const actions: JobAction = {
                    actionName: req.body.actionName,
                    jobId: req.body.jobId,
                    isSuccess: req.body.isSuccess,
                    notes: req.body.notes,
                    timeOfAction: new Date(),
                    userId: req.authenticate.userId,
                    actionId: uuid(),
                    ownerId: req.authenticate.userId,
                    actionType: req.body.actionType
                }
                req.data = actions;
                await jobActionQuery.create(req);
                resolve({status: "success", message: strings.jobActionCreatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary Create job actions in bulk, used internally
    * and is not exposed to the api
    * @param req
    * @return success status
    */
    public async createBulk(req: ExpressRequest, jobId: Array<any>) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const bulkArray: any = [[]];
                jobId.forEach(job => {
                    const actionX = [
                        "Assigned the job",
                        job.jobId,
                        true,
                        "",
                        new Date(),
                        req.authenticate.userId,
                        uuid(),
                        req.authenticate.userId,
                        "Assign"
                    ];
                    bulkArray[0].push(actionX);
                });

                req.data = {actions: bulkArray}
                jobActionQuery.bulkCreate(req);
                resolve({status: "success", message: ""});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary get all actions of a job
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.jobId - the jobId of which to get the actions for
	 * @returns { Response } an array of actions in the res.data
	 */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["jobId"]);
                req.data = {jobId: req.body.jobId}
                const actions = await jobActionQuery.get(req);
                resolve({status: "success", data: {actions}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary Update an action
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.actionId - the actionId to update
	 * @returns { Response } strings.jobActionUpdatedSuccess
	 */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["actionId"]);
                req.data = {
                    actionId: req.body.actionId,
                    update: {
                        notes: req.body.notes
                    }
                }
                await jobActionQuery.update(req);
                resolve({status: "success", message: strings.jobActionUpdateSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
    
    /**
	 * @public
	 * @async
	 * @summary Delete a job action
	 * @param { ExpressRequest} req - the default requets object
     * @param { string } req.body.actionId
	 * @returns { Response } strings.jobActionDeletedSuccess
	 */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["actionId"]);
                req.data = {actionId: req.body.actionId}
                await jobActionQuery.delete(req);
                resolve({status: "success", message: strings.jobActionDeleteSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default JobActionClass;