export interface IdealPostcodeAddress {
    result:{
        postcode: string,
        postcode_inward: string,
        postcode_outward: string,
        post_town: string,
        dependant_locality: string,
        double_dependant_locality: string,
        thoroughfare: string,
        dependant_thoroughfare: string,
        building_number: string,
        building_name: string,
        sub_building_name: string,
        po_box: string,
        department_name: string,
        organisation_name: string,
        udprn: number,
        umprn : string,
        postcode_type: string,
        su_organisation_indicator: string,
        delivery_point_suffix: string,
        line_1: string,
        line_2: string,
        line_3: string,
        premise: string,
        county:  string,
        administrative_county:  string,
        postal_county:  string,
        traditional_county: string,
        district: string,
        ward: string,
        country: string,
        longitude: string,
        latitude: string,
        eastings: number,
        northings: number
    },
    code: number,
    message: string
}

export default IdealPostcodeAddress;
