import query from "../middleware/query";
import { ExpressRequest } from "../models/express";
import { ContactPerson } from "../models/contactPepole";

/**
 * @class
 * @classdesc Query class to manage contact people
 * @author Mo Gold
 */
export class ContactPepoleQuery {

	/**
	 * @public
	 * @async
	 * @summary Create a contact person
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const create = await query("insert into contactPeople set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Create bulk contact people
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<ContactPerson> } req.data.contactPeople - an array of contact people
	 * @returns an empty promise
	 */
    public async createBulk(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const createBulk = await query("insert into contactPeople (firstName, lastName, emailAddress, contactId) values ?;", req.data.contactPeople);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
	}
	
	/**
	 * @public
	 * @async
	 * @summary Update a contact person
	 * @param { ExpressRequest } req - the default request object
	 * @param { String } req.data.contactId - a contact person Id to update
	 * @param { Object } req.data.update - an object with the properties to update
	 * @returns an empty promise
	 */
	public async update(req: ExpressRequest) {
		return new Promise(async(resolve, reject) => {
			try {
				const update = await query(`update contactPeople set ? where contactId = "${req.data.contactId}";`, req.data.update);
				resolve()
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Get contact peolple by customers
	 * @param { ExpressRequest } req - the default request object
	 * @param { String } req.data.customerId - a customerId to filter by
	 * @returns { Array<ContactPerson> } an array of contact people
	 * the result can return an empty array if there are no contact people
	 * attached to the customer
	 */
	public async getByCustomer(req: ExpressRequest) {
		return new Promise<Array<ContactPerson>>(async(resolve, reject) => {
			try {
				const get = await query(`select * from contactPeople where customerId = "${req.data.customerId}";`, null, {canReturnMultiple: true, canReturnZero: true});
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Get a single contact person by Id
	 * @param { ExpressRequest } req - the default request object
	 * @param { String } req.data.contactId - a contactId to filter by
	 * @returns { Array<ContactPerson> } an array with a single contact person
	 */
	public async getSingle(req: ExpressRequest) {
		return new Promise<Array<ContactPerson>>(async(resolve, reject) => {
			try {
				const get = await query(`select * from contactPeople where contactId = "${req.data.contactId}";`, null);
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Get contact people with a custom query
	 * @param { String } queryString - a valid sql string
	 * @returns { Array<ContactPerson> } an array of contact people
	 */
	public async getCustom(queryString: string) {
		return new Promise<Array<ContactPerson>>(async(resolve, reject) => {
			try {
				const get = await query(queryString, null, {canReturnZero: true, canReturnMultiple: true});
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Delete a contact person
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<Address> } req.data.contactId - a contactId to delete
	 * @returns an empty promise
	 */
	public async delete(req: ExpressRequest) {
		return new Promise(async(resolve, reject) => {
			try {
				const deleteQuery = await query(`delete from contactPeople where contactId = "${req.data.contactId}";`, {isDelete: true})
				resolve()
			} catch(error) {
				reject(error);
			}
		});
	}

}

export default ContactPepoleQuery;