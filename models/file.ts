export interface File {
    [key: string]: any,
    fileId: string,
    fileType: string,
    fileName: string,
    fileExtension: string,
    fileOwner: string,
    fileTags?: string
}

export default File;