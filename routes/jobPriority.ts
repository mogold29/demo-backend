import { ExpressRequest } from "../models/express";
import { JobPriority } from "../models/jobPriority";
import { JobPriorityClass } from "../logic/jobPriority";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

const jobPriorityClass = new JobPriorityClass();

routes.post("/jobpriorities/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "priorities");
    	const create = await jobPriorityClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/jobpriorities/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "priorities", req.body.priorityId);
    	const update = await jobPriorityClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/jobPriorities/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "priorities");
    	const get = await jobPriorityClass.get();
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;