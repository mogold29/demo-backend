import  { Request } from "express";

export interface ExpressRequest extends Request {
    data?: any;
    authenticate?: {
        sessionToken?: string;
        userId?: string;
        deviceId?: string;
    };
    ipAddress?: string,
    permission?: string;
    file?: any;
};
