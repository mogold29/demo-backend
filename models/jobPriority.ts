export interface JobPriority {
    [key: string]: any,
    priorityName: string,
    priorityColor?: string,
    priorityId: string,
    isAlerting: boolean,
    priorityOrder: number
}

export default JobPriority;