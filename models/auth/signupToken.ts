export interface SignupToken {
    userId: string,
    signupToken: string,
    ownerId: string
};