export interface PreviousPassword {
    userId: string,
    password: string,
    ownerId: string
}