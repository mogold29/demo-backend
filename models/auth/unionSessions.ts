import { Session } from "./session";
import { OldSession } from "./oldSession";

export interface UnionSession {
    sessions: Session[],
    oldSessions: OldSession[]
}