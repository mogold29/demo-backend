export interface Log {
    actionName: string,
    attribute: string,
    isSuccess: boolean,
    userId: string,
    ipAddress: string,
    logId: string,
    sessionToken: string,
    logTime: number,
    actionId?: string,
    ownerId: string
};

export interface BasicLog {
    actionName: string,
    attribute: string,
    isSuccess: boolean,
    actionId?: string,
};