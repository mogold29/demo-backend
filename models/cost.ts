export interface Cost {
    [key: string]: any,
    jobId: string,
    userId: string,
    description?: string,
    cost: number,
    category: string,
    approved?: boolean,
    rejected?: boolean,
    dateSubmitted: Date,
    dateApprovedRejected?: Date,
    approveRejectedByUser?: string,
    costId: string,
    ownerId?: string
}

export default Cost;