import { ExpressRequest } from "../../models/express";
import query from "../../middleware/query";
import { User } from "../../models/auth/user";

/**
 * @class
 * @description The query class for users
 * @author Mo Gold
 */
export class UserQuery {

    /**
     * @async
     * @description Create a new user
     * @param {object} req - the default request object
     * @param {User} req.data - an object with the new user details
     * @returns An empty promise
     */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const createQuery = await query("insert into users set ?", req.data, {});
                resolve();
            } catch(error) {
                console.log(error)
                reject(error);
            }
        });        
    }

    /**
     * @async
     * @description get a single user from a provided userId
     * @param {object} req - default request object
     * @param {string} req.data.userId - a userId
     * @returns {Array} an array with a single user
     */
    public async getSingleUser(req: ExpressRequest) {
        return new Promise<Array<User>>(async(resolve, reject) => {
            try {
                const getQuery: any = await query(`select * from users where userId = "${req.data.userId}";`, null, {});
                delete getQuery[0].password;
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        });
    }

    /**
     * @async
     * @description gets a user from a provided email address
     * @param {object} req - default request object
     * @param {string} req.data.emailAddress - an email address to filter by
     * @returns {Array} an array with a single user
     */
    public async getByEmail(req: ExpressRequest) {
        return new Promise<Array<User>>(async(resolve, reject) => {
            try {
                const getQuery: any = await query("select * from users where emailAddress = '" + req.data.emailAddress + "';", null, {});
                delete getQuery[0].password;
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        });
    }

    public async getWithPassword(req: ExpressRequest) {
        return new Promise<Array<User>>(async(resolve, reject) => {
            try {
                const getQuery: any = await query("select * from users where userId = '" + req.data.userId + "';", null, {});
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }

    public async getWithPasswordByEmail(req: ExpressRequest) {
        return new Promise<Array<User>>(async(resolve, reject) => {
            try {
                const getQuery: any = await query("select * from users where emailAddress = '" + req.data.emailAddress + "';", null, {});
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }
    
    /**
     * @async
     * @description gets users using advanced predfined filters, used for paging and sorting etc
     * @param {string} queryString - a valid mysql query string
     * @returns {Array} an array of users
     */
    public async getCustom(queryString: string) {
        return new Promise<Array<User>>(async(resolve, reject) => {
            try {
                const getQuery: any = await query(queryString, null, {canReturnZero: true, canReturnMultiple: true});

                getQuery.forEach((user: User) => {
                    delete user.password;
                });

                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
    * @public
    * @async
    * @summary Get all users
    * @param req - the default request obejct
    * @return { Array<User> } an array of users
    */
    public async getAll(req: ExpressRequest) {
        return new Promise<Array<User>>(async(resolve, reject) => {
        	try {
        		const get = await query("select * from users", null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
     * @description updates a user from provided object
     * @param {object} req - the default request object
     * @param {object} req.data.update - an object with the properties to update
     * @param {string} req.data.userId - a string with the userId
     * @returns an empty promise
     */
    public async updateUser(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const updateQuery = await query("update users set ? where userId = '" + req.data.userId + "';", req.data.update, {});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @async
     * @description delete a user
     * @param {object} req - the default req object
     * @param {string} req.data.userId - a userId
     * @returns an emptry promise
     */
    public async deleteUser(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const deleteQuery = await query("delete from users where userId = '" + req.data.userId + "';", null, {});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    public async getCountOfUsers(queryString: string) {
        return new Promise(async(resolve, reject) => {
            try {
                const countQuery = await query(queryString, null);
                resolve(countQuery);
            } catch(error)  {
                reject(error);
            }
        })
    }

    /**
    * @public
    * @async
    * @summary Activate a user
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async activateUser(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update users set active = true where userId = "${req.data.userId}";`, null, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary Deactivate a user
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async deactivateUser(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update users set active = false where userId = "${req.data.userId}";`, null, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default UserQuery;