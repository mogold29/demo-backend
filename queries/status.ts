import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { Status } from "../models/status";

/**
 * @class
 * @author Mo Gold
 * @classdesc StatusQuery is the query class for statsus
 */
export class StatusQuery {

	/**
	 * @public
	 * @async
	 * @summary Create a new status
	 * @param { ExpressRequest} req - the default requets object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const create = await query("insert itno status set ?;", req.data);
        		resolve();
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Get all status
	 * @param { ExpressRequest} req - the default requets object
	 * @returns an array of statsus
	 */
    public async get() {
        return new Promise<Array<Status>>(async(resolve, reject) => {
        	try {
        		const get = await query("select * from status;", null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(get);
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default StatusQuery;