import query from "../middleware/query";
import { ExpressRequest } from "../models/express";
import { Address } from "../models/address";

/**
 * @class
 * @classdesc Query class to manage addresses
 * @author Mo Gold
 */
export class AddressQuery {

	/**
	 * @public
	 * @async
	 * @summary Create a new address
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest){
        return new Promise<void>(async(resolve, reject) => {
        	try {
                const create = await query("insert into addresses set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Get a single address by addressId
	 * @param { ExpressRequest } req - the default request object
	 * @param { string } req.data.addressId - an addressId
	 * @returns { Array<Address> } an array of with one address
	 */
    public async getSingleByUdprn(req: ExpressRequest) {
        return new Promise<Array<Address>>(async(resolve, reject) => {
        	try {
                const get = await query(`select * from addresses where udprn = "${req.data.udprn}";`, null, {canReturnZero: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Get addresses with a custom query string
	 * @param { string } queryString - a valid sql string
	 * @returns { Array<Address> } an array of addresses
	 */
    public async getCustom(queryString: string) {
        return new Promise<Array<Address>>(async(resolve, reject) => {
        	try {
        		const queryName = await query(queryString, null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(queryName)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Update an address
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<Address> } req.data.addressId - an addressId
	 * @param { Object } req.data.update - an object with the updates properties
	 * @returns an empty promise
	 */
    public async update(req: ExpressRequest) {
        return new Promise<void>(async(resolve, reject) => {
        	try {
        		await query(`update addresses set ? where udprn = "${req.data.udprn}";`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
				console.log(error);
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Activate an address
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<Address> } req.data.addressId - an addressId to activate
	 * @returns an empty promise
	 */
    public async activate(req: ExpressRequest) {
        return new Promise<void>(async(resolve, reject) => {
        	try {
        		await query(`update addresses set active = true where addressId = "${req.data.addressId}";`, null);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Deactivate an address
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<Address> } req.data.addressId - an addressId to deactivate
	 * @returns an empty promise
	 */
    public async deactivate(req: ExpressRequest) {
        return new Promise<void>(async(resolve, reject) => {
        	try {
        		await query(`update addresses set active = false where addressId = "${req.data.addressId}";`, null)
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Delete an address
	 * @param { ExpressRequest } req - the default request object
	 * @param { Array<Address> } req.data.addressId - an addressId to delete
	 * @returns an empty promise
	 */
    public async delete(req: ExpressRequest) {
        return new Promise<void>(async(resolve, reject) => {
        	try {
        		await query(`delete from addresses where addressId = "${req.data.addressId}";`, null, {isDelete: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default AddressQuery;