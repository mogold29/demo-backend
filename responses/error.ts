import { Response } from "express";
/**
 * @function responseError
 * @summary Function to send a response with a file
 * @param res {object} - default express response object
 * @param data {any} - an error message, could be anything includng strings, functions, objects & numbers
 * @example responses.error(res, `error message`);
 */
export default function(res: Response, data?: any) {
    if(!data) {
        res.status(401).send({data: "There was an error, that's all we know"});
    } else if(data.message) {
        res.status(401).send({data: data.message});
    } else {
        res.status(401).json({data: data.toString()});
    }
}