import { ExpressRequest } from "../../models/express";
import query from "../../middleware/query";
import { Permission } from "../../models/permission";

/**
 * @class
 * @description The query class for prmission
 * @author Mo Gold
 */
export class PermissionQuery {

    /**
     * @async
     * @description Create a new permission
     * @param {object} req - default req object
     * @returns an empty promise
     */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const createQuery = await query("insert into permissions set ? ", req.data, {});
                resolve()
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @description get all permissions
     * @param req - default request object
     * @returns an array of permissions
     */
    public async get(req: ExpressRequest) {
        return new Promise<Array<Permission>>(async(resolve, reject) => {
            try {  
                const getQuery:any = await query("select * from permissions;", null, {});
                resolve(getQuery)
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
    * @public
    * @async
    * @summary Get a single permission
    * @param { ExpressRequest } req - the default request object
    * @return an array with a single permission
    */
    public async getById(req: ExpressRequest) {
        return new Promise<Array<Permission>>(async(resolve, reject) => {
        	try {
        		const get: any = await query(`select * from permissions where permissionId = "${req.data.permissionId}";`, null)
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary get all permissions of a role
    * @param { ExpressRequest } req - the default request object
    * @return an array of permissions
    */
    public async getByRole(req: ExpressRequest) {
        return new Promise<Array<Permission>>(async(resolve, reject) => {
        	try {
        		const get: any = await query(`select * from permissions where roleId = "${req.data.roleId}";`, null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary get permissions with custom sort and filter options 
    * @param { string } queryString a valid mySql query
    * @return an array with results, can e empty
    */
    public async getCustom(queryString: string) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const get = await query(queryString, null, {canReturnZero: true, canReturnMultiple: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
     * @async
     * @description update a permission
     * @param {object} req - the default request object
     * @param {Permission} req.data.update - an object with the properties to update
     * @param {string} req.data.permissionId - a permissionId
     * @returns an empty promise
     */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                 const updateQuery = await query("update permissions set ? where permissionId = '" + req.data.permissionId + "';", req.data.update, {isUpdate: true});
                 resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @async
     * @description delete a permission
     * @param {object} req - the default request object
     * @param {string} req.data.permissionId - the permissionId to delete
     * @returns empty promise
     */
    public async delete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const deleteQuery = await query("delete from permissions where permssionId = '" + req.data.permissionId + "';", null, {isDelete: true});
                resolve();
            } catch(error) {   
                reject(error);
            }
        })
    }
}