import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { NotesClass } from "../logic/notes";
const notesClass = new NotesClass();

routes.post("/notes/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "notes");
    	const create = await notesClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/notes/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "notes");
    	const get = await notesClass.get(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/notes/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "notes", req.body.noteId);
    	const update = await notesClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/notes/delete", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "delete", "notes", req.body.noteId);
    	const deleteFunction = await notesClass.delete(req);
    	Responses.success(res, deleteFunction);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;