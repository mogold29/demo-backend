import query from "../middleware/query";
import { ExpressRequest } from "../models/express";
import { JobPriority } from "../models/jobPriority";

/**
 * @class
 * @author Mo Gold
 * @classdesc JobPriorityQuery is the query class for job priorities
 */
export class JobPriorityQuery {

	/**
	 * @param
	 * @async
	 * @summary Create a new priortiy
	 * @param { ExpressRequest } req - the default requst object
	 * @returns an empty promsie
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
                const createQuery = await query("insert into jobPriorities set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @param
	 * @async
	 * @summary update a priority
	 * @param { ExpressRequest } req - the default request obejct
	 * @param { Object } req.data.update - an object with the properties to update
	 * @param { string } req.data.priorityId - the priortityId to update
	 * @returns an empty promise
	 */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const updateQuery = await query(`update jobPriorities set ? where priorityId = "${req.data.priorityId}";`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary get all priorities
	 * @returns { Array<JobPriority> } an array of priorities
	 */
    public async get() {
        return new Promise<Array<JobPriority>>(async(resolve, reject) => {
        	try {
        		const getQuery = await query("select * from jobPriorities;", null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(getQuery)
        	} catch(error) {
        		reject(error);
        	}
        });
	}
	
	/**
	 * @public
	 * @async
	 * @summary get a single priority
	 * @param { ExpressRequest } req - the default requets object
	 * @returns { Array<JobPriority> } an array with one priority
	 */
	public async getSingle(req: ExpressRequest) {
        return new Promise<Array<JobPriority>>(async(resolve, reject) => {
        	try {
        		const getQuery = await query(`select * from jobPriorities where priorityId = "${req.data.priorityId}";`, null);
        		resolve(getQuery)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Delete a priority
	 * @param { ExpressRequest } req - the default request obejct
	 * @returns an empty promise
	 */
    public async delete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const deleteQuery = await query(`delete from jobPriorities where priorityId = "${req.data.priorityId}"`, null);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default JobPriorityQuery;