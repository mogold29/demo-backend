import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { Customer } from "../models/customer";
import { CustomerClass } from "../logic/customer";
import { ExpressRequest } from "../models/express";
const customerClass = new CustomerClass();

describe("Customer Test", () => {
    
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);
    const randomNumber = Math.floor(100000000 + Math.random() * 900000000);;

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        }
    };
    let res: any = {};

    describe("Create customer", () => {
        it("should create a customer successfully", async() => {
            
            const customer: Customer = {
                addressLineOne: "123 Main Street",
                addressPostcode: "M72BB",
                companyName: uuid(),
                addressCity: "Test City",
                addressBorugh: random,
                udprn: randomNumber,
                longitude: randomNumber,
                latitude: randomNumber,
                active: true,
                companyNumber: uuid(),
                customerId: uuid(),
                ownerId: uuid(),
                officeEmail: "test@domain.com",
                officePhone: "0000000000000"
            }

            req.body = customer;
            
            const result = await customerClass.create(req);
            assert.equal(result.message, strings.customerSuccess);
            assert.equal(result.status, "success");
        });

        it("should error when missing required fieled", async() => {
            const customer = {
                addressPostcode: "M72BB",
                companyName: uuid(),
                addressCity: "Test City",
                active: true,
                companyNumber: uuid(),
                customerId: uuid(),
                ownerId: uuid(),
                officeEmail: "test@domain.com",
                officePhone: "0000000000000"
            }

            req.body = customer;
            
            const result = await customerClass.create(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "property addressLineOne is missing and is required, please check and try again")
            });
        });

        it("should error when existing company number", async() => {
            const customer = {
                addressLineOne: "!34 Main street",
                addressPostcode: "M72BB",
                companyName: uuid(),
                addressCity: "Test City",
                active: true,
                companyNumber: 1234567,
                customerId: uuid(),
                ownerId: uuid(),
                officeEmail: "test@domain.com",
                officePhone: "0000000000000"
            }

            req.body = customer;
            
            const result = await customerClass.create(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "😵 You have tried to use an existing value in a filed which must be unique")
            });
        })
    });

    describe("Create bulk customers", () => {
        it("Should create customers from array", async() => {
            const customers: Customer[] = [
                {
                    addressLineOne: "123 Main Street - Bulk 1",
                    addressPostcode: "M72BB",
                    companyName: uuid(),
                    addressCity: "Test City",
                    active: true,
                    companyNumber: uuid(),
                    customerId: uuid(),
                    ownerId: uuid(),
                    officeEmail: "test@domain.com",
                    officePhone: "0000000000000",
                    udprn: randomNumber,
                    latitude: randomNumber,
                    longitude: randomNumber,
                    addressBorugh: random
                },
                {
                    addressLineOne: "123 Main Street - Bulk 2",
                    addressPostcode: "M72BB",
                    companyName: uuid(),
                    addressCity: "Test City",
                    active: true,
                    companyNumber: uuid(),
                    customerId: uuid(),
                    ownerId: uuid(),
                    officeEmail: "test@domain.com",
                    officePhone: "0000000000000",
                    udprn: randomNumber,
                    latitude: randomNumber,
                    longitude: randomNumber,
                    addressBorugh: random
                }
            ];

            req.body = {customers}

            const create = await customerClass.createBulk(req);
            assert.equal(create.message, strings.bulkCustomerSuccess);
        });

        it("should error only affected customer with error when bulk upload", async() => {
            const customers = [
                {
                    addressLineOne: "123 Main Street - Bulk2 1",
                    addressPostcode: "M72BB",
                    companyName: uuid(),
                    addressCity: "Test City",
                    active: true,
                    customerId: uuid(),
                    ownerId: uuid(),
                    officeEmail: "test@domain.com",
                    officePhone: "0000000000000"
                },
                {
                    addressLineOne: "123 Main Street - Bulk2 2",
                    addressPostcode: "M72BB",
                    companyName: uuid(),
                    addressCity: "Test City",
                    active: true,
                    companyNumber: uuid(),
                    customerId: uuid(),
                    ownerId: uuid(),
                    officeEmail: "test@domain.comxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                    officePhone: "0000000000000"
                }
            ];

            req.body = {customers}

            const create = await customerClass.createBulk(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "Property companyNumber is missing at position 0")
            })
        })
    });

    describe("Get customers", () => {
        it("should get customer by id", async() => {
            req.body = {customerId: "f7c001cf-b37e-4c43-a7fe-69c73728d72d"}
            const get = await customerClass.getSingleById(req);
            assert.isArray(get.data.customers);
        });

        it("should get customers from empty custom queries", async() => {
            const get = await customerClass.get(req);
            assert.isArray(get.data.customers);
        });
        it("should get a non existing customer and return empty array", async() => {
            req.body = {customerId: "uuuu"}
            customerClass.getSingleById(req).catch((error: any) => {
                expect(() => {throw error}).to.throw("Invalid");
            })
        });
        it("should get a customer by name", async() => {
            req.body = {companyName: "Test company"}
            const get = await customerClass.getSingleByName(req);
            assert.equal(get.status, "success");
            assert.isArray(get.data.responseArray);
        });
        it("should get a customer by non-existing name", async() => {
            req.body = {companyName: "Texstt company"}
            const get = await customerClass.getSingleByName(req);
            assert.equal(get.status, "error");
        });
        it("should get reps of a customer", async() => {
            req.body = {customerId: "3e438251-10ab-4106-af81-17a34b00a80c"}
            const get = await customerClass.getReps(req);
            assert.isArray(get.data.reps);
        })
    });

    describe("Update customers", () => {
        const randomName = random;
        it("should update customer successfully", async() => {
            req.body = {
                customerId: "3e438251-10ab-4106-af81-17a34b00a80c",
                companyName: randomName
            }
            const update = await customerClass.update(req);
            assert.equal(update.status, "success");
        });
        it("should error updating if no customerId provided", async() => {
            req.body = {companyName: "Not updating comany name"}
            const update = await customerClass.update(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "property customerId is missing and is required, please check and try again")
            });
        });
        it("should return success with message of no updated properties when no properties have changed", async() => {
            req.body = {
                customerId: "3e438251-10ab-4106-af81-17a34b00a80c",
                companyName: randomName
            }
            const update = await customerClass.update(req);
            assert.equal(update.message, strings.noUpdatedProperties);
        });
        it("should activate a customer", async() => {
            req.body = {customerId: "3e438251-10ab-4106-af81-17a34b00a80c"}
            const activate = await customerClass.activate(req);
            assert.equal(activate.message, strings.customerActivatedSuccess);
        });
        it("should deactivate a customer", async() => {
            req.body = {
                customerId: "3e438251-10ab-4106-af81-17a34b00a80c",
                deactivationReason: "i dont know im just testing the system"
            }
            const activate = await customerClass.deactivate(req);
            assert.equal(activate.message, strings.customerDeactivatedSuccess);
        });
        it("should update notes successfully", async() => {
            req.body = {
                customerId: "3e438251-10ab-4106-af81-17a34b00a80c",
                notes: "abc test notes update;"
            }
            const updateNotes = await customerClass.updateNotes(req);
            assert.equal(updateNotes.data.notes, "abc test notes update;");
        });
    });

    describe("Customer files", () => {
        it("should get customer files", async() => {
            req.body = {customerId: "3e438251-10ab-4106-af81-17a34b00a80c"}
            const files = await customerClass.getFiles(req);
            assert.isArray(files.data.files);
        });
    })


})