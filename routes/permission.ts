import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { PermissionClass } from "../logic/auth/permission";
const permissionClass = new PermissionClass();

routes.post("/permission/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "permission");
    	const create = await permissionClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/permission/getbyrole", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "permissions");
		const get = await permissionClass.getByRoleId(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/permission/get", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "permissions");
		const get = await permissionClass.get(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/permission/update", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "update", "permission", req.body.permissionId);
		const update = await permissionClass.update(req);
		Responses.success(res, update);
	} catch(error) {
		Responses.error(res, error);
	}
});

export default routes;