export interface OldSession {
    userId: string,
    sessionToken: string,
    sessionExpiry: string,
    sessionIp: string,
    sessionDeviceId: string,
    sessionDateCreated: string,
    sessionDateEnded: string,
    ownerId: string
};