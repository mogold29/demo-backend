export interface Status {
    [key: string]: any,
    status: number,
    statusDescription: string,
    statusOrder: number
}

export default Status;