import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import { NotesQuery } from "../queries/notes";
import { Note } from "../models/notes";

const notesQuery = new NotesQuery();

export class NotesClass {

    /**
    * @public
    * @async
    * @summary create a note
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.title - the note title
    * @param { string } req.body.content - the note content
    * @param { string } req.body.attribute - the attribute the note is for
    * such as customer or job
    * @param { string } req.body.attributeId - the id of the attribute such
    * as the custmerId or the jobId
    * @return success message inf the res.message
    */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["title", "content", "attribute", "attributeId"]);
                const note: Note = {
                    title: req.body.title,
                    content: req.body.content,
                    createdBy: req.authenticate.userId,
                    createdDatetime: new Date(),
                    attribute: req.body.attribute,
                    attributeId: req.body.attributeId,
                    noteId: uuid()
                }

                req.data = note;
                await notesQuery.create(req);
                resolve({status: "success", message: strings.notesCreatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary get all notes for a single attribute such as
    * all notes for a single job
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.attribute - the attribute such as job or custmer
    * @param { string } req.body.attributeId - the id of the attribute such as 
    * customerId or jobId
    * @return { Array<Note> } an array of notes
    */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["attribute", "attributeId"]);
                req.data = {
                    attribute: req.body.attribute,
                    attributeId: req.body.attributeId
                }
                const notes = await notesQuery.get(req);
                resolve({status: "success", data: notes});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary update a single note
    * @param { ExpressRequest } req - the default request object
    * @param { string } req.body.noteId - the noteId to update
    * @return success message in res.message
    */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["noteId"]);
                req.data = {noteId: req.body.noteId}
                const existing = await notesQuery.getSingle(req);
                const updated: any = {}
                for(let[key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        updated[key] = req.body[key]
                    }
                }
                
                if(Object.keys(updated).length < 1) {
                    resolve({status: "error", message: strings.noUpdatedProperties});
                }

                req.data = {
                    update: updated,
                    noteId: req.body.noteId
                }

                await notesQuery.update(req);
                resolve({status: "success", message: strings.notesUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
    * @public
    * @async
    * @summary delete a single note
    * @param { ExpressRequest } req - te default request object
    * @param { string } req.body.noteId - the noteId to delete
    * @return a success message in the res.message
    */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["noteId"]);
                req.data = {noteId: req.body.noteId}
                await notesQuery.delete(req);
                resolve({status: "success", message: strings.noteDeletedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default NotesClass;