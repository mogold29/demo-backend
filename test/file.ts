import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { File } from "../models/file";
import { FileClass } from "../logic/file";
import { CustomerClass } from "../logic/customer";

import query from "../middleware/query";

const fileClass = new FileClass();
const customerClass = new CustomerClass();

describe("File tests", async() => {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        body: {}
    };
    let res: any = {};
    const customerId = "007012d4-3636-4d0e-ae55-6c719550aa9a";

    describe("get files", async() => {
        it("should get files", async() => {
            const create = await query(`insert into files values("68edd37d-eb9e-4f22-9c11-9827d0d45deee", "customer", "randomFile", ".pdf", "007012d4-3636-4d0e-ae55-6c719550aa9a");`, null);
            req.body = {
                fileType: "customer",
                fileOwner: customerId
            }
            const getFiles = await fileClass.get(req);
            assert.isArray(getFiles.data.files);
            assert.equal(getFiles.status, "success");
        })
    });

    describe("delete files", async() => {
        req.body = {
            fileType: "customer",
            fileOwner: customerId
        }
        const existing = await fileClass.get(req);
        req.body = {
            fileId: existing.data.files[1].fileId,
            fileOwner: customerId
        }
        const deleteFile = await fileClass.delete(req);
        assert.equal(deleteFile.status, "success");
    })


})