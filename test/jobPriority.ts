import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { JobPriority } from "../models/jobPriority";
import { JobPriorityClass } from "../logic/jobPriority";
const jobPriorityClass = new JobPriorityClass();

describe("Job Priority Tests", async() => {

    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        }
    };
    let res: any = {};

    describe("Create priority", async() => {
        it("should create a priority successfully", async() => {
            const priority: JobPriority = {
                priorityId: uuid(),
                priorityName: random,
                priorityColor: "#GGGGGG",
                isAlerting: false,
                priorityOrder: 5
            }
            req.body = priority;
            const create = await jobPriorityClass.create(req);
            assert.equal(create.message, strings.priorityCreatedSuccessfully);
        });
        it("should error when priority already exists", async() => {
            const existing = await jobPriorityClass.get();
            const priority: JobPriority = {
                priorityId: uuid(),
                priorityName: existing.data.priorties[0].priorityName,
                priorityColor: "#JJJJJJ",
                isAlerting: false,
                priorityOrder: 5
            }
            const create = await jobPriorityClass.create(req);
            assert.equal(create.message, strings.priorityAlreadyExists);
        });
    });

    describe("Update prioerties", async() => {
        it("should update prioerites successfully", async() => {
            const existing = await jobPriorityClass.get();
            const updatePriority = {
                priorityId: existing.data.priorties[0].priorityId,
                priorityName: `random${random}`
            }
            req.body = updatePriority;
            const update = await jobPriorityClass.update(req);
            assert.equal(update.message, strings.priorityUpdatedSuccess);
        });
        it("should error when no changes", async() => {
            const existing = await jobPriorityClass.get();
            const updatePriority = {
                priorityId: existing.data.priorties[0].priorityId,
                priorityName: existing.data.priorties[0].priorityName
            }
            req.body = updatePriority;
            const update = await jobPriorityClass.update(req);
            assert.equal(update.message, strings.noUpdatedProperties);
        });
    });

    describe("Get priorities", async() => {
        it("get priorities should return array", async() => {
            const prioerites = await jobPriorityClass.get();
            assert.isArray(prioerites.data.priorties);
        });
    });

});