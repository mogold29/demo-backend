import { User } from "../models/auth/user";
import { Permission } from "../models/permission";
import { ExpressRequest } from "../models/express";

import { UserQuery as uq } from "../queries/auth/user";
const userQuery = new uq();

import query from "./query";

/**
 * @function permission
 * @summary Check if a user has permission to take action
 * @description This function checks if a user has permission to execute
 * a action such as 'read' or 'edit' on an attribute such as 'customers'
 * if the action is an edit function and as such the action will happen
 * on an existing record then a check is done based on the recordId, if
 * the user is allowed to modify records the user owns or is the user
 * allowed to modify records if the user is a manager of the owner
 * @param {ExpressRequest} req - the default request object
 * @param {string} action - the action the user wants to execute
 * @param {string} attribute - the attribute on which the user wants to take the action
 * @param {string} recordId - the recordId of the attribute if for example editing a record
 * @returns {boolean} true
 * @returns {string} "You do not have permission to do this action"
 */
export default function(req: ExpressRequest, action: string, attribute: string, recordId?: string) {
    return new Promise(async(resolve, reject) => {
        try {
            /**
             * if there is no session token or there is no userId then
             * we cannot check permissions against a users role
             */
            if (req.authenticate.sessionToken == undefined || req.authenticate.userId == undefined) {
                reject("You do not have permission to do this action");
            }

            req.data = {
                userId: req.authenticate.userId
            }

            const user = await userQuery.getSingleUser(req);
            const userRole = user[0].roleId;

            const permissionQuery: Permission[] = await query(`select * from permissions where roleId = '${userRole}' and action = '${action}' and attribute = '${attribute}';`, null);

            if(recordId) {
                /* 
                *  get the column name of the recordId such as userId, this is done by removing the 
                *  the last charcter of the attribute varibale which is always plural and appending Id 
                *  the result of the attribute 'users' would be 'userId'
                */
                let idColumn: string = `${attribute.substr(0, attribute.length - 1)}Id`;
                // ---------------------------------------------------
                // ---------------------------------------------------
                // this needs to be removed and the db schema update!!
                // ---------------------------------------------------
                // ---------------------------------------------------
                if (idColumn === "addresseId") {
                    idColumn = "addressId";
                }
                const recordQuery = await query(`select ownerId from ${attribute} where ${idColumn} = '${recordId}';`, null);
                const ownerOfRecord: User[] = await query(`select * from users where userId = '${recordQuery[0].ownerId}'`, null)

                /**
                 * if the session user is the owner of the record and the user can 
                 * do action on attribute ifOwner then resolve(true);
                 */
                if(permissionQuery[0].allowIfOwner && ownerOfRecord[0].userId === req.authenticate.userId) {
                    resolve(true)
                }

                /**
                 * if the session user is the manager of the record and the user can
                 * do action on attribute, then resolve(true);
                 */
                if(permissionQuery[0].allowIfManager && ownerOfRecord[0].userManager === req.authenticate.userId) {
                    resolve(true);
                }

                /**
                 * if session is allowed to do action on attribute regardless of Id 
                 * then resolve(true); otherwise reject("error message")
                 */
                if(permissionQuery[0].allow) {
                    resolve(true);
                } else {
                    reject("You do not have permission to do this action");
                }

            } else {
                /**
                 * if the permission requires a recordId then reject
                 */
                if(permissionQuery[0].requireId) {
                    reject("You do not have permission to do this action");
                }
                /**
                 * because there is no record is then we cannot check ifOwner and
                 * and ifManager as such we only check for allow and resolve or reject
                 */
                if(permissionQuery[0].allow) {
                    resolve(true);
                } else {
                    reject("You do not have permission to do this action");
                }
            }
        } catch (error) {
            reject(error);
        }
    });
}