import query from "../middleware/query";
import customer, { Customer } from "../models/customer";
import { ExpressRequest } from "../models/express";

/**
 * @class
 * @author Mo Gold
 * @classdesc CustomerQuery is the query class for customers
 */
export class CustomerQuery {

    /**
     * @public
     * @async
     * @summary Create a customer
     * @param { ExpressRequest } req - the default request object
     * @returns an empty promise 
     */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const createQuery = await query(`insert into customers set ?;`, req.data);
                resolve();
            } catch(error) {
                reject(error);
            }
        });
    }

    /**
     * @public
     * @async
     * @summary Create customers in bulk
     * @param { ExpressRequest } req - the default request object
     * @param { Array } req.body.customers - an array of customers
     * @returns an empty promise
     */
    public async createBulk(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const bulkQuery = await query(`insert into customers (companyName, addressLineOne, addressPostcode, addressCity, officePhone, officeEmail, companyNumber, active, customerId, ownerId) values ?;`, req.data.customers);
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get customers with a valid mysql query string
     * @param { string } queryString -  a valid mysql query
     * @returns { Array<Customers> } an array of customers, can be zero length
     */
    public async get(queryString: string) {
        return new Promise<Array<Customer>>(async(resolve, reject) => {
            try {
                console.log(queryString);
                const getQuery = await query(queryString, null, {canReturnMultiple: true, canReturnZero: true});
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get a customer by Id
     * @param { ExpressRequest } req - the default request object
     * @returns { Array<Customer> } an array with one customer
     */
    public async getSingleById(req: ExpressRequest) { 
        return new Promise<Array<customer>>(async(resolve, reject) => {
            try {
                let getQuery = await query(`select * from customers where customerId = "${req.data.customerId}";`, null);
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Get customers filtered by name
     * @param { ExpressRequest } req - the default request object
     * @returns { Array<Customer> } an array of customers
     */
    public async getSingleByName(req: ExpressRequest) {
        return new Promise<Array<customer>>(async(resolve, reject) => {
            try {
                const getQuery = await query(`select * from customers where companyName like "%${req.data.companyName}%";`, null, {canReturnZero: true, canReturnMultiple: true});
                resolve(getQuery);
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Update a customer
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.data.customerId - the customer Id to update
     * @param { object } req.data.update - an object with the properties to update
     * @returns an empty promise
     */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const updateQuery = await query(`update customers set ? where customerId = "${req.data.customerId}";`, req.data.update);
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @param
     * @async
     * @summary Activate a customer
     * @param { ExpressRequest } req - the default request object
     * @returns an empty promise 
     */
    public async activate(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const activateQuery = await query(`update customers set active = true where customerId = "${req.data.customerId}";`, null, {isUpdate: true});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Deactivate a customer
     * @param { ExpressRequest } req - the default request object
     * @returns an empty promise 
     */
    public async deactivate(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const deactivateQuery = await query(`update customers set active = false where customerId = "${req.data.customerId}";`, null, {isUpdate: true});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }

    /**
     * @public
     * @async
     * @summary Update customer notes
     * @param { ExpressRequest } req - the default request object
     * @returns an empty promise 
     */
    public async updateNotes(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
            try {
                const updateQuery = await query(`update customers set notes = "${req.data.notes}" where customerId = "${req.data.customerId}";`, null, {isUpdate: true});
                resolve();
            } catch(error) {
                reject(error);
            }
        })
    }
}

export default CustomerQuery;