import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { File } from "../models/file";

/**
 * @class
 * @author Mo Gold
 * @classdesc FileQuery is the query class for files
 */
export class FileQuery {

	/**
	 * @public
	 * @async
	 * @summary Create a new file in the database
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const create = await query("insert into files set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Get files filtered by owner and type
	 * @param { ExpressRequest } req - the default request object
	 */
    public async get(req: ExpressRequest) {
        return new Promise<Array<File>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select * from files where fileType = "${req.data.fileType}" and fileOwner = "${req.data.fileOwner}";`, null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
	}
	
	/**
	* @public
	* @async
	* @summary get a single file by Id
	* @param { ExpressRequest } req - the default request object
	* @return an array of files
	*/
	public async getSingle(req: ExpressRequest) {
		return new Promise<Array<File>>(async(resolve, reject) => {
			try {
				const get = await query(`select * from files where fileId = "${req.data.fileId}"`, null);
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Delete a file from the databse
	 * @param { ExpressRequest } req - the default request object
	 * @returns an empty promise
	 */
    public async delete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const deleteQuery = await query(`delete from files where fileId = "${req.data.fileId}" and fileOwner = "${req.data.fileOwner}";`, null, {isDelete: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default FileQuery;
