export interface Customer {
    [key: string] : any,
    companyName: string,
    addressLineOne: string,
    addressLineTwo?: string,
    addressLineThree?: string,
    addressPostcode: string,
    addressBorugh: string,
    addressCity: string,
    udprn: any;
    officePhone?: string,
    officeEmail?: string,
    companyNumber: string,
    active: boolean,
    notes?: string,
    deactivationReason?: string,
    deactivationNotes?: string,
    longitude: any,
    latitude: any,
    customerId: string,
    ownerId: string
}

export default Customer;