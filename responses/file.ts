/**
 * @function fileResponse
 * @summary Function to send a response with a file
 * @param res {object} - default express response object
 * @param data {string} - file paths where the file is located
 * @example responses.file(res, `/output/reports/quaterly`);
 */
export default function(res: any, data?:any) {
    if(!data) {
        res.status(400);
        res.send("File not found")
    } else {
        res.status(200);
        res.download(data.data);
    }
}