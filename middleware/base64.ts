const fs = require("fs");
/**
 * @class
 * @classdesc Convert file to base64 for emailing attachements
 */
export class Base64 {

    /**
    * @public
    * @async
    * @summary Generate a base64 string of the file
    * @param { string } filepath - node compatible file path
    * @return base64 string of file
    */
    public async encodeFile(filepath: string) {
        return new Promise<Response>(async (resolve, reject) => {
            const file = fs.readFileSync(filepath);
            const base64 = file.toString("base64");
            resolve(base64);
        });
    }
}

export default Base64;