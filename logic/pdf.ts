const pdf = require("html-pdf");

/**
 * @class
 * @classdesc Class to handle creating PDF files
 */
export class PdfClass {

    /**
    * @public
    * @async
    * @summary Generate and save a PDF file
    * @param { string } html - the html to use
    * @param { object } [opts] - options for file conifg
    * @param { string } saveTo - a relative location to save the file to
    * @return a valid node file path
    */
    public async generate(html: string, opts: any, saveTo: string) {
        return new Promise<Response>(async (resolve, reject) => {
        	if (!html) {
                reject("Missing HTML!");
            }
            if (!saveTo) {
                reject("Missing save to location");
            }
            pdf.create(html, opts).toFile(`${saveTo}`, function(err: any, filepath: any) {
                if (err) {
                    reject("An error occured during file creation")
                }
                resolve(filepath);
            });
        });
    }
}

export default PdfClass;