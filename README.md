# Demo Backend API

This is part of a backend currentlly used in production at Reeced, some of the code has been removed for security and privacy purposes


## Modules
The API provides access to the following modules
* Jobs
* Customers
* Users
* Costs
* Authentication
##### 
Each of the above modules has full documentation included, see the docs folder or run `npm run docs` to generate the documentation

## Installation

Use the package manager [npm](https://www.npmjs.com/get-npm) to install dependencies.

```bash
npm i
```
For development and testing please run
```bash
npm i --dev
```

Run `schema.sql` to set up the database no demo data is provided


## Running

```bash
npm run-script start
```

## Testing
```bash
npm run-script test
```

## Contributing
This is only a demo project and no active development is happening

Please make sure to update tests as appropriate.

## License
[GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
