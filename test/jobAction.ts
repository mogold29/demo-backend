import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { JobAction } from "../models/jobAction";
import { JobActionClass } from "../logic/jobAction";
const jobActionClass = new JobActionClass();

describe("Job Actions Tests", async() => {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        }
    };
    let res: any = {};
    const jobId = "66a4a231-42fd-430a-b68b-89c63de22948";

    describe("Create actions", async() => {
        it("should create an action successfully", async() => {
            const action: JobAction = {
                actionName: "call",
                actionId: uuid(),
                jobId,
                isSuccess: true,
                notes: uuid(),
                timeOfAction: new Date(),
                ownerId: req.authenticate.userId,
                actionType: "call"
            }
            req.body = action;
            const create = await jobActionClass.create(req);
            assert.equal(create.status, "success");
        });
        it("should create a priority with all required properties", async() => {
            const action: any = {
                actionName: "call",
                jobId,
                isSuccess: true,
                notes: uuid(),
            }
            req.body = action;
            const create = await jobActionClass.create(req);
            assert.equal(create.status, "success");
        });
        it("should error when missing required properties", async() => {
            const action: any = {
                jobId,
                isSuccess: true,
                notes: uuid(),
            }
            req.body = action;
            jobActionClass.create(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "property actionName is missing and is required, please check and try again")
            });
        });
    });

    describe("get job actions", async() => {
        it("should get all actions of a job", async() => {
            req.body = {jobId}
            const get = await jobActionClass.get(req);
            assert.equal(get.status, "success");
            assert.isArray(get.data.actions);
        });
    });

    describe("update job actions", async() => {
        it("should update an action", async() => {
            req.body = {jobId}
            const existing = await jobActionClass.get(req);
            const action = existing.data.actions[0].actionId;
            req.body = {
                actionId: action,
                notes: uuid()
            }
            const update = await jobActionClass.update(req);
            assert.equal(update.status, "success");
        });
    });

    describe("delete job actions", async() => {
        it("should delete an action", async() => {
            req.body = {jobId}
            const existing = await jobActionClass.get(req);
            const action = existing.data.actions[0].actionId;
            req.body = {actionId: action}
            const deleteAction = await jobActionClass.delete(req);
            assert.equal(deleteAction.status, "success");
        })
    })
})