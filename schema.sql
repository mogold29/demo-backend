create database demoBackendX1;
use demoBackendX1;

create table users (
    firstName varchar(250) not null,
    lastName varchar(250) not null,
    emailAddress varchar(100) not null unique,
    password  varchar(250) not null,
    phoneNumber varchar(20) unique,
    tfaEnabled boolean default false,
    tfaSecret varchar(250),
    forceChangePassword boolean default false,
    userId varchar(250) not null unique,
    roleId varchar(250)
);

create table roles (
    roleName varchar(50) not null,
    roleDescription varchar(250),
    roleId varchar(250) not null unique
);

create table permissions (
    actionName varchar(250) not null,
    roleId varchar(250) not null,
    allow boolean not null
);

create table sessions (
    userId varchar(250) not null,
    sessionToken varchar(250) not null unique,
    sessionExpiry datetime not null,
    sessionIp varchar(20) not null,
    sessionDeviceId varchar(250) not null,
    sessionDateCreated datetime not null
);

create table previousPasswords (
    userId varchar(250) not null,
    password varchar(250) not null
);


alter table permissions add column permissionId varchar(250) not null unique after allow;

create table oldSessions (
    userId varchar(250) not null,
    sessionToken varchar(250) not null unique,
    sessionExpiry datetime not null,
    sessionIp varchar(20) not null,
    sessionDeviceId varchar(250) not null,
    sessionDateCreated datetime not null,
    sessionDateEnded datetime not null
);

update version set version = 0.04;

create table attributes (
    attribute varchar(250) not null unique
);

create table actions (
    action varchar(250) not null unique
);

alter table permissions add column action varchar(250) not null;
alter table permissions add column attribute varchar(250) not null;
alter table permissions drop column actionName;
alter table permissions drop column allow;

alter table permissions add column allow boolean;
alter table permissions add column allowIfManager boolean;
alter table permissions add column allowIfOwner boolean;

alter table users add column ownerId varchar(250);
alter table signupTokens add column ownerId varchar(250);
alter table sessions add column ownerId varchar(250);
alter table previousPasswords add column add column ownerId varchar(250);
alter table logs add column ownerId varchar(250);
alter table forgotPassword add column ownerId varchar(250);
alter table oldSessions add column ownerId varchar(250);
alter table users add column userManager varchar(250);
alter table permissions add column requireId boolean;


alter table logs add column attribute varchar(250);

create table customers (
    companyName varchar(250) not null unique,
    addressLineOne varchar(250) not null,
    addressLineTwo varchar(250),
    addressLineThree varchar(250),
    addressPostcode varchar(10) not null,
    addressCity varchar(100) not null,
    officePhone varchar(50),
    officeEmail varchar(250),
    companyNumber varchar(250) not null unique,
    active boolean,
    notes varchar(2500),
    deactivationReason varchar(250),
    deactivationNotes varchar(2500),
    companyId varchar(250) not null unique,
    ownerId varchar(250) not null
);

create table jobPriorities (
    priorityName varchar(250) not null unique,
    priorityColor varchar(20),
    priorityId varchar(250) unique not null,
    isAlerting boolean
);

create table addresses (
    addressLineOne varchar(250) not null,
    addressLineTwo varchar(250),
    addressLineThree varchar(250),
    addressPostcode varchar(20) not null,
    addressCity varchar(100) not null,
    addressBorugh varchar(250),
    parkingNotes varchar(250),
    parkingZone varchar(10),
    isEstateParking boolean,
    isActive boolean,
    addressId varchar(250) not null unique,
    customerId varchar(250) not null,
    ownerId varchar(250)
);

create table contactPeople (
    firstName varchar(250) not null,
    lastName varchar(250) not null,
    emailAddress varchar(250) not null,
    mobileNumber varchar(250),
    officeExtension varchar(20),
    favouriteTopics varchar(2500),
    isDefault boolean,
    contactId varchar(250) not null unique,
    customerId varchar(250),
    ownerId varchar(250)
);

create table files (
    fileId varchar(250) not null unique,
    fileType varchar(250) not null,
    fileName varchar(250) not null,
    fileExtension varchar(10) not null,
    fileOwner varchar(250) not null
);

create table jobActions (
    actionName varchar(250) not null,
    jobId varchar(250) not null,
    isSuccess boolean,
    notes varchar(250),
    timeOfAction datetime,
    userId varchar(250),
    actionId varchar(250) not null unique,
    ownerId varchar(250)
);

create table costs (
    jobId varchar(250) not null,
    userId varchar(250) not null,
    description varchar(250),
    cost float not null,
    category varchar(250) not null,
    approved boolean,
    rejected boolean,
    dateSubmitted datetime,
    dateApprovedRejected datetime,
    approveRejectedByUser varchar(250),
    costId varchar(250) not null unique,
    ownerId varchar(250)
);

create table status (
    status int not null unique,
    statusDescription varchar(250) not null,
    statusOrder int not null unique
);

create table jobs (
    customerId varchar(250) not null,
    currentCustomerName varchar(250) not null,
    currentCustomerAddressLineOne varchar(250) not null,
    currentCustomerAddressLineTwo varchar(250),
    currentCustomerAddressLineThree varchar(250),
    currentCustomerAddressPostcode varchar(20) not null,
    currentCustomerAddressCity varchar(50) not null,
    addressId varchar(250) not null,
    category varchar(250) not null,
    priorityId varchar(250) not null,
    tenantFirstName varchar(250) not null,
    tenantLastName varchar(250) not null,
    tenantPhoneNumberOne varchar(20),
    tenantPhoneNumberTwo varchar(20),
    tenantPhoneNumberThree varchar(20),
    tenantPhoneNumberFour varchar(20),
    tenantEmailAddress varchar(250),
    accessDetails varchar(250),
    keyLocation varchar(250),
    jobDescription varchar(2500),
    jobNotes varchar(2500),
    customerContactPersonId varchar(250),
    isCancelled boolean,
    isCompleted boolean,
    isInvoiced boolean,
    quote float,
    status int,
    jobCreationDate date,
    jobEndedDate date,
    jobId varchar(250) not null unique,
    ownerId varchar(250)
);

create table version (
    version float
);

insert into version set version = 0.01;

create table assignment (
    userId varchar(250) not null,
    jobId varchar(250) unique not null,
    dateAssigned datetime,
    assignedByUser varchar(250) not null,
    isCompleted boolean,
    arrivedOnSiteDate datetime,
    completedOnSiteDate datetime,
    locationArrivedOnSite varchar(30),
    locationCompletedOnSite varchar(30)
);

update version set version = 0.02;

alter table jobs add column cancelledReason text;
alter table jobs add column cancelledBy varchar(250);
alter table jobs add column addressLatitude varchar(10);
alter table jobs add column addressLongitude varchar(10);
alter table jobs add column completionApprovedBy varchar(250);

update version set version = 0.03;

alter table addresses add column udprn varchar(10) unique;
alter table addresses add column longitude varchar(20);
alter table addresses add column latitude varchar(20);
alter table addresses drop customerId;

update version set version = 0.04;

create table notes (
    title varchar(250) not null,
    content text not null,
    createdBy varchar(250) not null,
    createdDatetime datetime not null,
    attribute varchar(250) not null,
    attributeId varchar(250) not null,
    noteId varchar(250) not null unique
);

alter table jobs add column addressLineOne varchar(250) not null;
alter table jobs add column addressLineTwo varchar(250);
alter table jobs add column addressLineThree varchar(250);
alter table jobs add column addressPostcode varchar(250) not null;
alter table jobs add column addressLineBorugh varchar(250) not null;
alter table jobs add column udprn varchar(15) not null;

alter table customers add column addressBorugh varchar(250);
alter table customers add column udprn varchar(20);
alter table customers add column latitude varchar(100);
alter table customers add column longitude varchar(100);
alter table jobs modify tenantLastName varchar(250) default null;
alter table jobPriorities add column priorityOrder int not null;

update version set version = 0.05;

alter table assignment add column tenantConfirmedDate date;
alter table assignment add column tenantConfirmedTime varchar(20);
alter table jobActions add column actionType varchar(20);
alter table status add column statusOrderText varchar(200);
alter table customers change companyId customerId varchar(250) not null unique;
alter table previousPasswords add column ownerId varchar(250);
alter table users add column active boolean;

update version set version = 0.06;

alter table users add column shortId int auto_increment unique;
alter table jobs add column cancelledNotes text;

update version set version = 0.07;

alter table jobs add column dueDate date;
alter table files add column fileTags varchar(250);
alter table users add column isContractor boolean default false;

update version set version = 0.08;