export interface RoleAndPermission {
    roleId: string,
    permissionId: string,
    lineId: string
}