import { ExpressRequest } from "../../models/express";
import query from "../../middleware/query";
import { Role } from "../../models/auth/role";

export class RoleQuery {

    /**
    * @public
    * @async
    * @summary Create a new role
    * @param { ExpressRequest } req - the default request obejct
    * @return an empty promise
    */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query("insert into roles set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary Get all roles
    * @return An array of roles, can return an empty array
    */
    public async get() {
        return new Promise<Array<Role>>(async(resolve, reject) => {
        	try {
        		const get: Role[] = await query("select * from roles;", null, {canReturnZero: true, canReturnMultiple: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary get a single role
    * @param { ExpressRequest } req - the default request object
    * @return an array with a single role
    */
    public async getSingleRole(req: ExpressRequest) {
        return new Promise<Array<Role>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select * from roles where roleId = "${req.data.roleId}";`, null, {canReturnZero: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary Update a role
    * @param { ExpressRequest } req - The default request object
    * @return an empty promise
    */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update roles set ? where roleId = "${req.data.roleId}";`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default RoleQuery;