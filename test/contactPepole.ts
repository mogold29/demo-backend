import { assert, expect} from "chai";
import "mocha";
import { uuid } from "uuidv4";
import strings from "../strings";

import { ContactPerson } from "../models/contactPepole";
import { ContactPeopleClass } from "../logic/contactPepole";
import { Customer } from "../models/customer";
import { CustomerClass } from "../logic/customer";

const contactPeopleClass = new ContactPeopleClass();
const customerClass = new CustomerClass();

describe("Contact people tests", async() => {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 5);

    let req: any = {
    	headers: {
    		sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        authenticate: {
            userId: "393bf149-b2c2-4b1e-a7aa-babb4ae9b83b",
            sessionToken: "bf52e7dd-e233-4f4d-9f47-f439970d3500"
        },
        body: {}
    };
    let res: any = {};
    let customerId: any;

    describe("Create contact people", async() => {
        it("should create a contact person", async() => {
            const customers = await customerClass.get(req);
            customerId = customers.data.customers[0].customerId;
            const contactPerson: ContactPerson = {
                firstName: random,
                lastName: random,
                emailAddress: random,
                mobileNumber: random,
                officeExtension: "123",
                favouriteTopics: random,
                isDefault: false,
                contactId: uuid(),
                ownerId: req.authenticate.userId,
                customerId: customerId
            }
            req.body = contactPerson;
            const create = await contactPeopleClass.create(req);
            assert.equal(create.message, strings.contactPersonCreatedSuccess);
            assert.equal(create.status, "success");
        });
        it("should create a contact person with only required fieleds", async() => {
            req.body = {
                firstName: "random",
                lastName: "random"
            }
            const create = await contactPeopleClass.create(req);
            assert.equal(create.message, strings.contactPersonCreatedSuccess);
            assert.equal(create.status, "success");
        });
        it("should error when missing required fieled", async() => {
            req.body = {firstName: random}
            contactPeopleClass.create(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "property lastName is missing and is required, please check and try again")
            });
        });
    });

    describe("Create bulk contact people", async() => {
        it("should create bulk contact people", async() => {
            const contactPeople = [
                {
                    firstName: random,
                    lastName: random,
                    emailAddress: random
                },
                {
                    firstName: random,
                    lastName: random,
                    emailAddress: random
                },
                {
                    firstName: random,
                    lastName: random,
                    emailAddress: random
                }
            ];
            req.body = {contactPeople}
            const create = await contactPeopleClass.createBulk(req);
            assert.equal(create.message, strings.bulkContactPersonCreatedSuccess);
            assert.equal(create.status, "success");
        });
        it("should error when missing required fieled", async() => {
            const contactPeople = [
                {
                    firstName: random,
                    lastName: random,
                    emailAddress: random
                },
                {
                    firstName: random,
                    lastName: random
                },
                {
                    firstName: random,
                    lastName: random,
                    emailAddress: random
                }
            ];
            req.body = {contactPeople}
            contactPeopleClass.createBulk(req).catch((error: any) => {
                expect(() => {throw error}).to.throw(Error, "Property emailAddress is missing at position 1")
            });
        });
    });

    describe("Update contact people", async() => {
        let existingId: any;
        let existingName: any;
        it("should update a contact person successfully", async() => {
            const existing = await contactPeopleClass.get(req);
            existingId = existing.data.contactPeople[0].contactId;
            req.body = {
                contactId: existingId,
                firstName: `${random}+++>>+${random}`
            }
            existingName = req.body.firstName;
            const update = await contactPeopleClass.update(req);
            assert.equal(update.message, strings.contactPersonUpdatedSuccess);
            assert.equal(update.status, "success");
        });
        it("should fail to update when no changed properties", async() => {
            req.body ={
                contactId: existingId,
                firstName: existingName

            }
            const update = await contactPeopleClass.update(req);
            assert.equal(update.message, strings.noUpdatedProperties);
            assert.equal(update.status, "error");
        });
    });

    describe("Get contact people", async() => {
        let existingId: any;
        it("should get a single contact person", async() => {
            const existing = await contactPeopleClass.get(req);
            existingId = existing.data.contactPeople[0].contactId;
            req.body = {contactId: existingId}
            const get = await contactPeopleClass.getSingle(req);
            assert.isArray(get.data.contactPeople);
            assert.equal(get.status, "success");
        });
        it("should get all contact people linked to a customer", async() => {
            req.body = {customerId}
            const get = await contactPeopleClass.getByCustomer(req);
            assert.isArray(get.data.contactPeople);
            assert.equal(get.status, "success");
        });
        it("should get conatct people with custom query", async() => {
            const get = await contactPeopleClass.get(req);
            assert.isArray(get.data.contactPeople);
            assert.equal(get.status, "success");
        });
    });

    describe("delete a conatact person", async() => {
        it("should delete a contact persons", async() => {
            const existing = await contactPeopleClass.get(req);
            const existingId = existing.data.contactPeople[0].contactId;
            req.body = {contactId: existingId}
            const deleteContactPerson = await contactPeopleClass.delete(req);
            assert.equal(deleteContactPerson.message, strings.contactPersonDeleted);
            assert.equal(deleteContactPerson.status, "success");
        });
    });
});