export interface Assignment {
    [key: string]: any,
    userId: string,
    jobId: string,
    dateAssigned: Date,
    assignedByUser: string,
    isCompleted?: boolean,
    arrivedOnSiteDate?: Date,
    completedOnSiteDate?: Date,
    locationArrivedOnSite?: string,
    locationCompletedOnSite?: string,
    tenantConfirmedDate?: string,
    tenantConfirmedTime?: string
}
export default Assignment;