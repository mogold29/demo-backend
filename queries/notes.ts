import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { Note } from "../models/notes";

/**
 * @class NotedQuery
 * @classdesc This class is the queries for notes
 */
export class NotesQuery {

    /**
    * @public
    * @async
    * @summary create a note
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query("insert into notes set ?;", req.data);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary get all notes for a single attributeId
    * @param { ExpressRequest } req - the default request object
    * @return { Array<Note> }> an array of notes
    */
    public async get(req: ExpressRequest) {
        return new Promise<Array<Note>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select notes.*, users.firstName, users.lastName from notes join users on users.userId = notes.createdBy where attribute = "${req.data.attribute}" and attributeId = "${req.data.attributeId}" order by createdDatetime desc;`, null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(get);
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary get a single note, genrally used for
    * checking exisiting note when updating
    * @param { ExpressRequest } req - the default request object
    * @return { Array<Note> } an array with a single note
    */
    public async getSingle(req: ExpressRequest) {
        return new Promise<Array<Note>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select * from notes where noteId = "${req.data.noteId}";`, null);
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary update a single note
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update notes set ? where noteId = "${req.data.noteId}";`, req.data.update, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary delete a single note
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async delete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`delete from notes where noteId = "${req.data.noteId}";`, null, {isDelete: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default NotesQuery;