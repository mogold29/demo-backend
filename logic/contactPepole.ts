import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import bulkRequestCheck from "../middleware/bulkRequestCheck";
import { ContactPerson } from "../models/contactPepole";
import { ContactPepoleQuery } from "../queries/contactPepole";

const contactPeopleQuery = new ContactPepoleQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc ContactPeopleClass manages all logic relating to contact people
 */
export class ContactPeopleClass {

    /**
     * @public
     * @async
     * @summary Create a contact person
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.firstName - the first name of the contact person
     * @param { stirng } req.body.lastName - the lastName of the contact person
     * @param { string } [req.body.emailAddress] - the email address of the contact person must be unique
     * @param { string } [req.body.mobileNumber] - the mobile number of the contact person
     * @param { string } [req.body.officeExtension] - the office extension phone number of the contact person
     * @param { string } [req.body.customerId] - a customerId to which the contact person is linked
     * @param { string } [req.body.favouriteTopics] - any notes on topics to discusss with the contact person
     * @param { boolean } [req.body.isDefault] - is this contact person the default contact for the customer
     * @returns { Response } strings.contactPersonCreatedSuccess
     */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
            try {
                const check = await requestCheck(req, ["firstName", "lastName"]);
                const contact: ContactPerson = {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    emailAddress: req.body.emailAddress || "",
                    mobileNumber: req.body.mobileNumber || "",
                    officeExtension: req.body.officeExtension || "",
                    contactId: uuid(),
                    customerId: req.body.customerId || "",
                    favouriteTopics: req.body.favouriteTopics.toString() || "",
                    isDefault: req.body.isDefault || false,
                    ownerId: req.authenticate.userId
                }
                req.data = contact;
                await contactPeopleQuery.create(req);
                resolve({status: "success", message: strings.contactPersonCreatedSuccess});
            } catch(error) {
                reject(error);
            };
        });
    }

    /**
     * @public
     * @async
     * @summary Create contact people in bulk
     * @param { ExpressRequest } req - the default request objet
     * @param { Arary<contactPeople> } req.body.contactPeople - an array of contact people
     * @param { stirng } req.body.contactPeople.firstName - the first name of the contact person
     * @param { string } req.body.contactPeople.lastName - the last name of the contcat person
     * @param { string } req.body.contactPeople.emailAddress - the email address of the contact person
     * @returns { Response } strings.bulkContactPersonCreatedSuccess
     */
    public async createBulk(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await bulkRequestCheck(req, ["firstName", "lastName", "emailAddress"], "contactPeople");
                const bulkArray: any = [[]];
                req.body.contactPeople.forEach((contact: ContactPerson) => {
                    let contactX = [
                        contact.firstName,
                        contact.lastName,
                        contact.emailAddress,
                        uuid()
                    ];
                    bulkArray[0].push(contactX);
                });
                req.data = {contactPeople: bulkArray}
                const createQuery = await contactPeopleQuery.createBulk(req);
                resolve({status: "success", message: strings.bulkContactPersonCreatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Update a contact person
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.contactId - the id of the contact person to update
     * @returns { Response } strings.contactPersonUpdatedSuccess
     */
    public async update(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["contactId"]);
                req.data = {contactId: req.body.contactId}
                const existing = await contactPeopleQuery.getSingle(req);
                let updated: any = {};
                for(let [key, val] of Object.entries(req.body)) {
                    if(existing[0][key] !== req.body[key]) {
                        updated[key] = req.body[key]
                    }
                }
                if(Object.keys(updated).length < 1) {
                    resolve({status: "success", message: strings.noUpdatedProperties});
                }
                req.data = {
                    update: updated,
                    contactId: req.body.contactId

                }
                const updateQuery = await contactPeopleQuery.update(req);
                resolve({status: "success", message: strings.contactPersonUpdatedSuccess});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get all contact people of a customer
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.customerId - the csutomerId of which to filter by
     * @returns { Response } an array of contact people in the res.data
     */
    public async getByCustomer(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["customerId"]);
                req.data = {customerId: req.body.customerId}
                const contactPeople = await contactPeopleQuery.getByCustomer(req);
                resolve({status: "success", data: {contactPeople}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get a single contact person
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.contactId - a contact person Id of which to filter
     * @returns { Response } an array with a single contacte person in the req.data
     */
    public async getSingle(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["contactId"]);
                req.data = {contactId: req.body.contactId}
                const contactPeople = await contactPeopleQuery.getSingle(req);
                resolve({status: "success", data: {contactPeople}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Get contact people with custom filters, sorting, and pagnation
     * @param { ExpressRequest } req - the default request object
     * @param { number } [req.body.opts.limit] - limit the number of results
     * @param { number } [req.body.opts.offset] - offset position
     * @param { string } [req.body.opts.orderBy] - how to order results ASC or DESC
     * @param { orderByField } [req.body.opts.orderByField] - the property to order by
     * @param { boolean } [req.body.opts.activated] - should the query return deactivated results
     * @returns { Response } an array of contact people and an array with the count of results in the res.data  
     */
    public async get(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
        		let limit: number = 0;
        		let offset: number = 10;
        		let orderBy: string = "ASC";
        		let orderByField: string = "lastName";
        		let queryString: string;
        		let countQueryString: string;
        		let activated = true;
    
        		if(req.body.opts) {
        			if(req.body.opts.offset != undefined) {offset = req.body.opts.offset};
        			if(req.body.opts.limit != undefined) {limit = req.body.opts.limit};
        			if(req.body.opts.orderBy != undefined) {orderBy = req.body.opts.orderBy};
        			if(req.body.opts.orderByField != undefined) {orderByField = req.body.opts.orderByField};  
        			if(req.body.opts.deactivated != undefined) {activated = req.body.opts.deactivated};
        		}
    
        		queryString = `select * from contactPeople order by ${orderByField} ${orderBy} limit ${limit}, ${offset};`;
        		countQueryString = `select count(lastName) as 'count' from contactPeople;`;
    
        		if(req.body.opts) {
        			if(req.body.opts.field != undefined) {
        				queryString = `select * from contactPeople and ${req.body.opts.field} like "%${req.body.opts.searchterm}%" order by ${orderByField} ${orderBy} limit ${limit}, ${offset};`;
        				countQueryString = `select count(addressLineOne) as 'count' from contactPeople and ${req.body.opts.field} like "%${req.body.opts.searchterm}%";`
        			}
        		}
    
        		const contactPeople = await contactPeopleQuery.getCustom(queryString);
        		const count = await contactPeopleQuery.getCustom(countQueryString);
        		resolve({status: "success", data: {contactPeople, count}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
     * @public
     * @async
     * @summary Delete a contact person
     * @param { ExpressRequest } req - the default request object
     * @param { string } req.body.contactId - the contact Id to delete
     * @returns { Response } strings.contactPersonDeleted
     */
    public async delete(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                await requestCheck(req, ["contactId"]);
                req.data = {contactId: req.body.contactId}
                await contactPeopleQuery.delete(req);
                resolve({status: "success", message: strings.contactPersonDeleted});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default ContactPeopleClass;