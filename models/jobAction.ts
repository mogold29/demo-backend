export interface JobAction {
    [key: string]: any,
    actionName: string,
    jobId: string,
    isSuccess: boolean,
    notes?: string,
    timeOfAction: Date,
    userId?: string,
    actionId: string,
    ownerId: string,
    actionType: string
}

export default JobAction;