import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { JobAction } from "../models/jobAction";
import { JobActionClass } from "../logic/jobAction"

const jobActionClass = new JobActionClass();

routes.post("/actions/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "actions");
    	const create = await jobActionClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/actions/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "actions", req.body.actionId);
    	const get = await jobActionClass.get(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/actions/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "actions", req.body.actionId);
    	const update = await jobActionClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/actions/delete", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "delete", "actions", req.body.actionId);
    	const deleteFundtion = await jobActionClass.delete(req);
    	Responses.success(res, deleteFundtion);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;