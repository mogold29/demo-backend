import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";

import express from "express";
const routes = express.Router();

import { SessionClass } from "../logic/auth/session";
const sessionClass = new SessionClass();

routes.post("/auth/check", async(req: ExpressRequest, res: any) => {
    try {
    	const session = await sessionClass.check(req);
    	Responses.success(res, session);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/auth/logout", async(req: ExpressRequest, res: any) => {
    try {   
    	const logout = await sessionClass.destroy(req);
    	Responses.success(res, logout);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;