import success from "./success";
import error from "./error";
import file from "./file";

export {success, error, file};