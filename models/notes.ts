export interface Note {
    [key: string]: any,
    title: string,
    content: string,
    createdBy: string,
    createdDatetime: Date,
    attribute: string,
    attributeId: string,
    noteId: string
}

export default Note;