export interface Role {
    [key: string]: any,
    roleName: string,
    roleId: string,
    roleDescription?: string
};