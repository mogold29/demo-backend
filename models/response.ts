export interface Response {
    status: string,
    message?: string,
    data?: any
}

enum Status {
    "success" = "success",
    "error" = "error"
}
