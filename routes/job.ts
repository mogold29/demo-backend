import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { JobClass } from "../logic/job";

const jobClass = new JobClass();

routes.post("/job/create", async(req: ExpressRequest, res: any) => {
    try {await Permission(req, "create", "jobs");
    	const create = await jobClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/get", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "jobs", req.body.jobId);
		const get = await jobClass.get(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/job/getCustom", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "getCustom", "jobs");
		const get = await jobClass.getCustom(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/job/assign", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "assign", "jobs", req.body.jobId);
    	const assign = await jobClass.assignJob(req);
    	Responses.success(res, assign);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/cancel", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "cancel", "jobs", req.body.jobId);
    	const cancel = await jobClass.cancel(req);
    	Responses.success(res, cancel);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/delete", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "delete", "jobs", req.body.jobId);
    	const deleteJob = await jobClass.delete(req);
    	Responses.success(res, deleteJob);
    } catch(error) {
		console.log(error);
    	Responses.error(res, error);
    }
});

routes.post("/job/arrivedOnSite", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "onsite", "jobs", req.body.jobId);
    	const arrive = await jobClass.arriveOnSite(req);
    	Responses.success(res, arrive);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/onSiteCompleted", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "onsite", "jobs", req.body.jobId);
    	const complete = await jobClass.onSiteComplete(req);
    	Responses.success(res, complete);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/officeReviewdAccepted", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "review", "jobs", req.body.jobId);
    	const review = await jobClass.officeReviewdApproved(req);
    	Responses.success(res, review);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/invoice", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "invoice", "jobs", req.body.jobId);
    	const invoice = await jobClass.invoice(req);
    	Responses.success(res, invoice);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/job/report", async(req: ExpressRequest, res: any) => {
    try {
		await Permission(req, "get", "jobs", req.body.jobId);
		
		const get = await jobClass.genrateReport(req);
		
		if (get.data) {
			Responses.file(res, {data:`${get.data.filename}`});
		} else if (get.message) {
			Responses.success(res, get);
		}
    } catch(error) {
		console.log(error);
    	Responses.error(res, error);
    }
});

routes.post("/job/update", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "update", "jobs", req.body.jobId);
		const update = await jobClass.update(req);
		Responses.success(res, update);
	} catch(error) {
		Responses.error(res, error);
	}
});

export default routes;