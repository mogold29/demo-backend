export default function(datey?: Date) {

    let date = new Date();
    if(datey) {date = new Date(datey)};
    
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getUTCFullYear()}`;

}