import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { Status } from "../models/status";
import { StatusClass } from "../logic/status";

const statusClass = new StatusClass();

routes.post("/status/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "su", "supers");
    	const create = await statusClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/status/get", async(req: ExpressRequest, res: any) => {
    try {
    	const get = await statusClass.get();
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;