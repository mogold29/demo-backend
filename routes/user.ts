import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { UserClass } from "../logic/auth/user";
const userClass = new UserClass();

routes.post("/users/get", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "users");
		const get = await userClass.getAll(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/users/getcustom", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "users");
		const get = await userClass.getCustom(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/users/getsingle", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "get", "users", req.body.userId);
		const get = await userClass.get(req);
		Responses.success(res, get);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/users/create", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "create", "users");
		const create = await userClass.create(req);
		Responses.success(res, create);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/users/activate", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "activate", "users", req.body.userId);
		const activate = await userClass.activate(req);
		Responses.success(res, activate);
	} catch(error) {
		Responses.error(res, error);
	}
});

routes.post("/users/deactivate", async(req: ExpressRequest, res: any) => {
	try {
		await Permission(req, "deactivate", "users", req.body.userId);
		const deactivate = await userClass.deactivate(req);
		Responses.success(res, deactivate);
	} catch(error) {
		Responses.error(res, error);
	}
});

export default routes;