import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { Job } from "../models/job";

/**
 * @class
 * @author Mo Gold
 * @classdesc JobQuery is the class for job queries
 */
export class JobQuery {

	/**
	 * @public
	 * @async
	 * @summary Create a new job
	 * @param { ExpressRequest} req - the default requets object
	 * @returns an empty promise
	 */
    public async create(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		const create = await query("insert into jobs set ?;", req.data);
        		resolve()
        	} catch(error) {
				console.log(error);
        		reject(error);
        	}
        });
    }

	/**
	 * @public
	 * @async
	 * @summary Get summary of a job
	 * @param { ExpressRequest} req - the efault requets object
	 * @returns an array with multiple data points
	 */
    public async getSummaryByCustomer(req: ExpressRequest) {
        return new Promise<Array<Job>>(async(resolve, reject) => {
        	try {
        		const get = await query(`select jobs.jobId, addresses.addressLineOne, addresses.addressCity, addresses.borugh, addresses.postcode, status.statusDescription, jobs.creationDate, jobs.jobEndedDate from jobs join job on jobs.addressId = addresses.addressId join status on status.status = jobs.status where jobs.customerId = "${req.data.customerId}";`, null)
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
	}
	
	/**
	 * @public
	 * @async
	 * @summary Get jobs with custom query string
	 * @param { stirng } queryString - a valid mysql query string
	 * @returns an array of jobs
	 */
	public async get(queryString: string) {
		return new Promise<Array<Job>>(async(resolve, reject) => {
			try {
				const get = await query(queryString, null, {canReturnMultiple: true, canReturnZero: true});
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	* @public
	* @async
	* @summary Get a job by jobId
	* @param { ExpressRequest } req - the default request object
	* @returns { Array<Job> } an array of with a single job 
	*/
	public async getById(req: ExpressRequest) {
		return new Promise<Array<Job>>(async(resolve, reject) => {
			try {
				const get = await query(`select * from jobs where jobId = "${req.data.jobId}";`, null);
				resolve(get)
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Update a job
	 * @param { ExpressRequest} req - the default requets object
	 * @returns an empty promise
	 */
	public async update(req: ExpressRequest) {
		return new Promise(async(resolve, reject) => {
			try {
				const update = await query(`update jobs set ? where jobId = "${req.data.jobId}";`, req.data.update, {isUpdate: true});
				resolve()
			} catch(error) {
				reject(error);
			}
		});
	}

	/**
	 * @public
	 * @async
	 * @summary Delete a job
	 * @param { ExpressRequest} req - the default requets object
	 * @returns an empty promise
	 */
	public async delete(req: ExpressRequest) {
		return new Promise(async(resolve, reject) => {
			try {
				const deleteQuery = await query(`delete from jobs where jobId = "${req.data.jobId}"`, null, {isDelete: true});
				resolve()
			} catch(error) {
				reject(error);
			}
		});
	}
}

export default JobQuery;