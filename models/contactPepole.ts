export interface ContactPerson {
    [key: string]: any,
    firstName: string,
    lastName: string,
    emailAddress: string,
    mobileNumber: string,
    officeExtension: string,
    favouriteTopics: string,
    isDefault: boolean,
    contactId: string,
    customerId: string,
    ownerId: string
}

export default ContactPerson;