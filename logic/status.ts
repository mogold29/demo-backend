import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import { uuid } from "uuidv4";
import strings from "../strings";
import requestCheck from "../middleware/requestCheck";
import bulkRequestCheck from "../middleware/bulkRequestCheck";
import { Status } from "../models/status";
import { StatusQuery } from "../queries/status";

const statusQuery = new StatusQuery();

/**
 * @class
 * @author Mo Gold
 * @classdesc StatusClass manages statusus
 */
export class StatusClass {

    /**
	 * @public
	 * @async
	 * @summary Create a new status - THIS METHOD SHOULD NOT BE USED IN CLIENT FACING API's
	 * @param { ExpressRequest} req - the default requets object
     * @param { number } req.body.status - the status number, must be unique
     * @param { string } req.body.statusDesctiption - a short description for the status
     * @param { number } req.body.statusOrder - a number to order the statsus by 
	 * @returns { Response } strings.statusCreatedSuccess
	 */
    public async create(req: ExpressRequest) {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const check = await requestCheck(req, ["status", "statusDesctiption", "statusOrder"]);
                const status: Status = {
                    status: req.body.status,
                    statusDescription: req.body.statusDescription,
                    statusOrder: req.body.statusOrder
                }
                req.data = status;
                const create = await statusQuery.create(req);
                resolve({status: "success", message: strings.statusCreatedSuccess})
        	} catch(error) {
        		reject(error);
        	};
        });
    }

    /**
	 * @public
	 * @async
	 * @summary Get all statsus
	 * @param { ExpressRequest} req - the efault requets object
	 * @returns { Response } an array of statsus
	 */
    public async get() {
        return new Promise<Response>(async (resolve, reject) => {
        	try {
                const status = await statusQuery.get();
                resolve({status: "success", data: {status}});
        	} catch(error) {
        		reject(error);
        	};
        });
    }
}

export default StatusClass;