import query from "./query";
import { ExpressRequest } from "../models/express";
import { User } from "../models/auth/user";

/**
 * @class
 * @classdesc Check if a logged in user is the manager of another user
 * @param { ExpressRequest } req - the default request object 
 * @param { string } userId - the user to check
 * @example isManager(req, "abcderfghhjkut");
 * @returns either resolves if the user is a manger or rejects if nothing is found
 */
export async function isManager(req: ExpressRequest, userId: string) {
    return new Promise(async(resolve, reject) => {
        try {
            console.log(getManagers())
        } catch(error) {
            reject(error);
        }
        /*let isManager: string = "";
        // keep checking for managers if isManager is an empty string
        while (isManager.length < 1) {
            const managers = await getManagers(userId);
            if (managers) {
                const manager = managers.find(manager => manager.userManager === req.authenticate.userId);
                if (manager) {
                    isManager = manager;
                }
            } else {
                // otherwise reject and and break the loop
               reject();
               break;
            }  
        }
        // the loop has finished and a manager was found so resolve()
        resolve();*/
    });
}

async function getManagers() {
    const managers = await query(`select userManager, userId from users;`, null, {canReturnMultiple: true});
    let tree: any = [];
    let secondTree: any = [];
    while(managers.length > 0) {
        managers.forEach((user: User, index: number) => {
            if(tree[user.userManager]) {
                tree[user.userManager].push(user.userId);
                managers.splice(index, 1);
            } else {
                tree[user.userId] = [];
            }
        });
    }
    while(tree.length > 0) {
        tree.forEach((user: any, index: number) => {
            if(secondTree[user]) {
                secondTree[user].push(tree[user]);
                tree.splice(index, 1);
            } else {
                secondTree[user] = [];
            }
        });
    }
    console.log(secondTree)
    return tree;
}