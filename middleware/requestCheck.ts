import { ExpressRequest } from "../models/express";
/** 
* @function requestCheck
* @summary Checks if the req has a body and if the req.body conains required parameters compared to the fields param
* @param {Object} req - the default express request
* @param {Array} fields - a string array of required fields to check if they exist in the req.body 
**/

export default function(req: any, fields: Array<string>) {
    
    return new Promise<boolean>((resolve, reject) => {
        if(!req.body) {
            reject(new Error("req.body is missing"));
        }
        
        fields.forEach((field) => {
            if(!req.body.hasOwnProperty(field)) {
                reject(new Error(`property ${field} is missing and is required, please check and try again`));
            }
        });

        resolve(true);
    })
};