import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { ContactPerson } from "../models/contactPepole";
import { ContactPeopleClass } from "../logic/contactPepole";

const contactPeopleClass = new ContactPeopleClass();

routes.post("/contact/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "contacts");
    	const create = await contactPeopleClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/contact/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "contacts", req.body.contactId);
    	const update = await contactPeopleClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/contact/getSingle", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "contacts", req.body.contactId);
    	const get = await contactPeopleClass.getSingle(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/contact/getByCustomer", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "contacts");
    	const get = await contactPeopleClass.getByCustomer(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/contact/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "contacts");
    	const get = await contactPeopleClass.get(req);
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/contact/delete", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "delete", "contacts", req.body.contactId);
    	const deleteFunction = await contactPeopleClass.delete(req);
    	Responses.success(res, deleteFunction);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;