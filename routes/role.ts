import { ExpressRequest } from "../models/express";
import { Response } from "../models/response";
import * as Responses from "../responses/reponse";
import Permission from "../middleware/permission";

import express from "express";
const routes = express.Router();

import { RoleClass } from "../logic/auth/role";
const roleClass = new RoleClass();

routes.post("/roles/get", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "get", "roles");
    	const get = await roleClass.get();
    	Responses.success(res, get);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/roles/create", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "create", "roles");
    	const create = await roleClass.create(req);
    	Responses.success(res, create);
    } catch(error) {
    	Responses.error(res, error);
    }
});

routes.post("/roles/update", async(req: ExpressRequest, res: any) => {
    try {
    	await Permission(req, "update", "roles", req.body.roleId);
    	const update = await roleClass.update(req);
    	Responses.success(res, update);
    } catch(error) {
    	Responses.error(res, error);
    }
});

export default routes;