/**
 * @class
 * @interface
 * @classdesc Address interface
 */
import { AddressNumber } from "@ideal-postcodes/api-typings/index";
export interface Address {
    [key: string]: any,
    addressLineOne: string,
    addressPostcode: string,
    addressCity: string,
    isEstateParking?: boolean,
    addressId: string,
    ownerId: string,
    addressLineTwo?: string,
    addressLineThree?: string,
    addressBorugh: string,
    parkingNotes?: string,
    parkingZone?: string,
    isActive?: boolean,
    udprn: number,
    longitude: AddressNumber,
    latitude: AddressNumber
}

export default Address;