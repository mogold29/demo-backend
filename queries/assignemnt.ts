import { ExpressRequest } from "../models/express";
import query from "../middleware/query";
import { Assignment } from "../models/assignment";

/**
 * @class
 * @author Mo Gold
 * @classdesc AssignmentQuery is the query class for assignments
 */
export class AssignmentQuery {

    /**
    * @public
    * @async
    * @summary Get an assignemnt by job
    * @param { ExpressRequest } req - the default request object
    * @return an array with a single assignemnt
    */
    public async getByJob(req: ExpressRequest) {
        return new Promise<Array<Assignment>>(async(resolve, reject) => {
        	try {
        		const getQuery = await query(`select * from assignment where jobId = "${req.data.jobId}";`, null, {canReturnMultiple: true, canReturnZero: true});
        		resolve(getQuery);
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary Update an assingment
    * @param { ExpressRequest } req - the default request object
    * @return an empty promise
    */
    public async update(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update assignment set ? where jobId = "${req.data.jobId}";`, req.data.update);
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary Complete an assignment
    * @param { ExpressRequest } req - the default request obeject
    * @return an empty promise
    */
    public async complete(req: ExpressRequest) {
        return new Promise(async(resolve, reject) => {
        	try {
        		await query(`update assignment set isCompleted = true where jobId = "${req.data.jobId}";`, null, {isUpdate: true});
        		resolve()
        	} catch(error) {
        		reject(error);
        	}
        });
    }

    /**
    * @public
    * @async
    * @summary Recomend jobs to be assigned
    * @return an array of jobs mixed with priority data
    */
    public async recommend() {
        return new Promise<Array<any>>(async(resolve, reject) => {
        	try {
        		const get = await query("select jobs.*, jobPriorities.* from jobs left join jobPriorities on jobPriorities.priorityId = jobs.priorityId where jobs.status = 10 and jobs.isCancelled is null order by jobPriorities.priorityOrder asc, jobs.jobCreationDate asc;", null, {canReturnZero: true, canReturnMultiple: true});
        		resolve(get)
        	} catch(error) {
        		reject(error);
        	}
        });
    }
}

export default AssignmentQuery;